package com.sat.dap;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.regex;
import static com.mongodb.client.model.Indexes.ascending;
import static com.mongodb.client.model.Indexes.descending;
import static com.mongodb.client.model.Sorts.orderBy;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import org.bson.Document;

public class getData {
    
    public List<Document> mongoDevGteData(String devRgx, Instant dateTime, MongoCollection<Document> mongocollection, Connection conn, String siteID) throws SQLException, ParseException{
        DateFormat mongoDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        List<String> devList=new ArrayList<>();
        List<Document> docListLatest=new ArrayList<>();
        List<Document> docListFinal=new ArrayList<>();

//        Statement stmt=conn.createStatement();
//        String devDetailQ=null;
//        if(devRgx.contains("WMS")){
//            devDetailQ=String.format("SELECT wms_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s", siteID);  
//        }
//        if(devRgx.contains("Inverter")){
//            devDetailQ=String.format("SELECT inverter_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverter_list where plant_id=%s", siteID);  
//        }
//        if(devRgx.contains("SMB")){
//            devDetailQ=String.format("SELECT SCB_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_list where plant_id=%s", siteID);  
//        }
//        if(devRgx.contains("MFM")){
//            devDetailQ=String.format("SELECT M_ID FROM swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_list where plant_id=%s", siteID);  
//        }
//        ResultSet devDetailRs=stmt.executeQuery(devDetailQ);
//        int devCount=0;
//        while(devDetailRs.next()){
//            devCount++;
//        }

//        try{
//            docList= (List<Document>) mongocollection.find(and(gte("ts", mongoDateFormat.parse(dateTime)), regex("device",devRgx))).sort(orderBy(descending("ts"))).into(new ArrayList<>());
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        
        try{
            docListLatest= (List<Document>) mongocollection.find(and(gte("ts", dateTime), regex("device",devRgx))).sort(orderBy(descending("ts"))).into(new ArrayList<>());
            //docListLatest= (List<Document>) mongocollection.find(and(gte("ts", dateTime), regex("device",devRgx))).sort(orderBy(descending("ts"))).limit(devCount).into(new ArrayList<>());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //to remove duplicate by device
        for(Document doc:docListLatest){
            if(!devList.contains(doc.get("device").toString())){
                docListFinal.add(doc);
                devList.add(doc.get("device").toString());
            }
        }

        return docListFinal;
    }
    
    
    public List<Document> mongoDevGteData1(String deviceName, Instant dateTime, MongoCollection<Document> mongocollection, Connection conn, String siteID) throws SQLException, ParseException{
        DateFormat mongoDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        List<Document> docList=new ArrayList<>();
        List<Document> docListFinal=new ArrayList<>();

        try{
            docList= (List<Document>) mongocollection.find(and(gte("ts", dateTime), eq("device",deviceName))).sort(orderBy(ascending("ts"))).into(new ArrayList<>());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //to remove duplicate by ts
        Date ts=new Date(0);
        for(Document doc:docList){
            if(ts!=doc.getDate("ts")){
                docListFinal.add(doc);
            }
            ts=doc.getDate("ts");
        }
        return docListFinal;
    }
    
    
    public List<Document> mongoDevBtwData(String deviceName, Instant dateTimeStart, Instant dateTimeStop, MongoCollection<Document> mongocollection, Connection conn, String siteID) throws SQLException, ParseException{
        List<Document> docList=new ArrayList<>();
        List<Document> docListFinal=new ArrayList<>();
        
        try{
            docList= (List<Document>) mongocollection.find(and(gte("ts", dateTimeStart), lt("ts", dateTimeStop), eq("device",deviceName))).sort(orderBy(ascending("ts"))).into(new ArrayList<>());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //to remove duplicate by ts
        Date ts=new Date(0);
        for(Document doc:docList){
            if(ts!=doc.getDate("ts")){
                docListFinal.add(doc);
            }
            ts=doc.getDate("ts");
        }
        
        return docListFinal;
    }

    
    //for all WMS which cumm are one
    public float irradianceBtw(Instant dateTimeStart,Instant dateTimeStop,String siteID,MongoCollection mongocollection,Connection conn) throws SQLException, ParseException{
        //get WMS irrediation
        List<String> wmsListCumm= new ArrayList<>();
        float radiation= 0.0f;
        
        Statement stmt1=conn.createStatement();
        String wmsDetailQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s and WMS_Cumulative=1;", siteID);  
        ResultSet wmsDetailRs=stmt1.executeQuery(wmsDetailQ);
        
        while(wmsDetailRs.next()){
            wmsListCumm.add(wmsDetailRs.getString("plant_id")+"_WMS_"+wmsDetailRs.getString("wms_id"));
        }

        List<Document> wmsDocList=mongoDevBtwData("WMS", dateTimeStart, dateTimeStop, mongocollection, conn, siteID);
        
        int r=0;
        int m=0;
        float rTot=0.0f;
        float mTot=0.0f;
        float valueHolderF=0.0f;
        Set keyset;
        for(Document doc:wmsDocList){
            if(wmsListCumm.contains(doc.getString("device"))){
                keyset=doc.keySet();
                try{
                    if(keyset.contains("w21")){
                        valueHolderF=Float.parseFloat(doc.get("w21").toString());
                        if(!Float.isNaN(valueHolderF)){
                            rTot=rTot+valueHolderF;
                            r++;
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }

                try{
                    if(keyset.contains("w22")){
                        valueHolderF=Float.parseFloat(doc.get("w22").toString());
                        if(!Float.isNaN(valueHolderF)){
                            rTot=rTot+valueHolderF;
                            r++;
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        if(r>0){
            radiation=rTot/r;
        }
        
        try{
            stmt1.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }
        
        return radiation/60000;
    }
    
    
    //for specific WMS
    public float irradianceBtw1(String device, Date dateTimeStart,Date dateTimeStop,String siteID,List<Document> wmsDocList,MongoCollection mongocollection,Connection conn) throws SQLException, ParseException{
        float radiation= 0.0f;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        int r=0;
        float rTot=0.0f;
        float valueHolderF=0.0f;
        Set keyset;
        for(Document doc:wmsDocList){
            if((dateTimeStart.compareTo(dateFormat.parse(doc.get("w23").toString()))<=0) && (dateTimeStop.compareTo(dateFormat.parse(doc.get("w23").toString()))>0)){
                keyset=doc.keySet();
                try{
                    if(keyset.contains("w21")){
                        valueHolderF=Float.parseFloat(doc.get("w21").toString());
                        if(!Float.isNaN(valueHolderF)){
                            rTot=rTot+valueHolderF;
                            r++;
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }

                try{
                    if(keyset.contains("w22")){
                        valueHolderF=Float.parseFloat(doc.get("w22").toString());
                        if(!Float.isNaN(valueHolderF)){
                            rTot=rTot+valueHolderF;
                            r++;
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if(r>0){
            radiation=rTot/r;
        }

        return radiation/60000;
    }
    
    
    public float lossEnergy(float estimatedPR, String siteID, String siteZone, String device, List<Document> docListMongo,MongoCollection mongocollection, Connection conn) throws SQLException, ParseException {
        //Plant, inverter, string level breakdown
        String todayD=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone(siteZone));
        String currDT=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        String wmsDailyQ=String.format("SELECT wdaily.wms_id,wdaily.poa_irradiance,wdaily.today_date,wdaily.plant_sunrise_time,wdaily.plant_sunset_time,wdaily.sunrise_Time_updated,wdaily.sunset_Time_updated FROM `Flexi_solar_wms_list` AS wlist JOIN `Flexi_solar_wms_daily` AS wdaily ON wdaily.plant_id = wlist.plant_id AND wdaily.wms_id=wlist.wms_id WHERE wlist.plant_id=%s AND wlist.WMS_Cumulative=1 AND wdaily.Today=%s;", siteID, todayD);  
        String eventDetQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_events where plant_id=%s and device_type in ('Plant', 'Inverter', 'SMB', 'String') and category not in (1,3,5) and error_code in (1000,2001,3000,7000) and (start_today=%s or end_today=%s or end_today is null);", siteID, todayD, todayD);
        ResultSet wmsDailyRs=stmt1.executeQuery(wmsDailyQ);
        ResultSet rsEvent=stmt2.executeQuery(eventDetQ);
        
        Date sunriseTime=null;
        Date sunsetTime=null;
        float lossEnergy=0.0f;
        float valueHolderF=0.0f;
        
        while(wmsDailyRs.next()){
            try{
                if(wmsDailyRs.getString("wms_id").contentEquals("1") && wmsDailyRs.getString("sunrise_Time_updated").contentEquals("1")){
                    try {
                        sunriseTime=dateFormat.parse(wmsDailyRs.getString("plant_sunrise_time"));
                    } catch (Exception ex) {
                        System.out.println("Catch inverter daily 1");
                    }
                }
            }
            catch (Exception ex) {    
            }
            
            try{
                if(wmsDailyRs.getString("wms_id").contentEquals("1") && wmsDailyRs.getString("sunset_Time_updated").contentEquals("0")){
                    try {
                        sunsetTime=dateFormat.parse(wmsDailyRs.getString("plant_sunset_time"));
                    } catch (Exception ex) {
                        System.out.println("Catch inverter daily 2");
                    }
                }
            }
            catch (Exception ex) {
            }
            
        }
        
        rsEvent.beforeFirst();
        while(rsEvent.next()){
            if(sunriseTime!=null){
                //add in breakdown time
                try{
                    if(sunsetTime==null && rsEvent.getString("end_date")==null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0){
                            try{
                                valueHolderF= irradianceBtw1(device,dateFormat.parse(rsEvent.getString("start_date")),dateFormat.parse(currDT), siteID, docListMongo, mongocollection, conn)*(Float.parseFloat(rsEvent.getString("dc_capacity")))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy1 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<0){
                            try{
                                valueHolderF= irradianceBtw1(device,sunriseTime,dateFormat.parse(currDT),siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                    }
                    else if(sunsetTime==null && rsEvent.getString("end_date")!=null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0){
                            try{
                                valueHolderF= irradianceBtw1(device,dateFormat.parse(rsEvent.getString("start_date")),dateFormat.parse(rsEvent.getString("end_date")),siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy3 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunriseTime)>=0){
                            try{
                                valueHolderF= irradianceBtw1(device,sunriseTime,dateFormat.parse(rsEvent.getString("end_date")),siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy4 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                    }
                    else if(sunsetTime!=null && rsEvent.getString("end_date")==null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunsetTime)<=0){
                            try{
                                valueHolderF= irradianceBtw1(device,dateFormat.parse(rsEvent.getString("start_date")),sunsetTime,siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy5 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0){
                            //plantBDMin=plantBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            System.out.println("i am here5 "+rsEvent.getString("start_date")+" "+sunriseTime+" "+rsEvent.getString("send_date")+" "+sunsetTime);
                            
                            try{
                                valueHolderF= irradianceBtw1(device,sunriseTime,sunsetTime, siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy6 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                    }
                    else if(sunsetTime!=null && rsEvent.getString("end_date")!=null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)<=0){
                            try{
                                valueHolderF= irradianceBtw1(device,dateFormat.parse(rsEvent.getString("start_date")),dateFormat.parse(rsEvent.getString("end_date")), siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy7 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunsetTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)>=0){
                            try{
                                valueHolderF= irradianceBtw1(device,dateFormat.parse(rsEvent.getString("start_date")),sunsetTime,siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy8 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunriseTime)>=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                try{
                                    valueHolderF= irradianceBtw1(device,sunriseTime,dateFormat.parse(rsEvent.getString("end_date")),siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                                }
                                catch(Exception ex){
                                    System.out.println("Catch in lossEnergy9 "+siteID);
                                }
                                if(!Float.isNaN(valueHolderF)){
                                    lossEnergy=lossEnergy+valueHolderF;
                                }
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)>=0){
                            try{
                                valueHolderF= irradianceBtw1(device,sunriseTime,sunsetTime,siteID,docListMongo,mongocollection,conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in lossEnergy10 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch lossenergy");
                }
            }
        }

        try{
            stmt1.close();
            stmt2.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }

        return lossEnergy;
    }
    
}

