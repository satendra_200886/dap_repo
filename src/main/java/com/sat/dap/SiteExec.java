package com.sat.dap;

import com.mongodb.client.MongoClient;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SiteExec{
    
    public static void main(String[] args) throws IOException{
        String mongouri = System.getenv("MONGODB_ATLAS_CLUSTER_URI");
        
        final String sqluri= "jdbc:mysql://cms-mysql.ct1jilk613s3.ap-south-1.rds.amazonaws.com/swlflexi_CMSswlawsReMACS";
        final String sqlid= "swl_cms_master";
        final String sqlpass= "EPUVJSXf2kbW6qLc";
//        final String sqluri= "jdbc:mysql://15.206.60.12:3306/swlflexi_CMSswlawsReMACS";
//        final String sqlid= "devuser";
//        final String sqlpass= "dev@sterlingandwilson";
        
        String currDir=new File("").getAbsolutePath();
        connection conn=new connection();

        final List<String> sitesInfo= conn.siteInfo(currDir);

        MongoClient mongoclient=conn.mongoConnection();
        dataProcess dataprocess= new dataProcess();
        
        int second=0;
        while(!(second==1)){
        second=LocalDateTime.now().getSecond();
        }
        
        Runnable runnable = () -> {
            final ExecutorService executor= Executors.newFixedThreadPool(70);
            try{
                final Connection sqlconn=DriverManager.getConnection(sqluri,sqlid,sqlpass);
                long startTime = System.currentTimeMillis();
                
                sitesInfo.forEach((sitesI) -> {
                    executor.submit(() -> {
                        dataprocess.deviceProcess(sitesI, mongoclient, sqlconn);
                    });
                });
                
                executor.shutdown();
                
                try {
                    if (!executor.awaitTermination(56, TimeUnit.SECONDS)) {
                        executor.shutdownNow();
                    }
                }
                catch (InterruptedException e) {
                    System.out.println("i am in terminationcatch");
                }

                try{
                    sqlconn.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                System.out.println("Time of excution "+(System.currentTimeMillis() - startTime));
            
            }catch(Exception e){
                System.out.println(e);
            }
        };
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.MINUTES);
    }

}