package com.sat.dap;

import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;
import com.luckycatlabs.sunrisesunset.dto.Location;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.eq;
import java.awt.geom.Arc2D;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.Document;


public class dailyParamProcess {
    getData getdata=new getData();
    sqlUpdateAndEntry sqlupdateandquery= new sqlUpdateAndEntry();
    
    public void wmsDaily(List<Document> docListMongoLatest, String siteID, String siteZone, Connection conn, MongoCollection mongocollection, Instant todayStartDT) throws SQLException, ParseException{
        List<Document> docListMongo=new ArrayList<>();
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String currDT=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String currD=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        String dateIDD=zoneddatetime.format(DateTimeFormatter.ofPattern("MMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat DOCFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        Statement stmt4= conn.createStatement();
        Statement stmt5= conn.createStatement();
        Statement stmt6= conn.createStatement();
        Statement stmt7= conn.createStatement();
//        Statement stmt8= conn.createStatement();
        String plantInfo=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_plant_info where id=%s;", siteID);  
        String wmsDailyQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily where plant_id=%s and today=%s order by wms_id asc;", siteID, todayD); 
        String mfmDailyQ=String.format("SELECT mlist.M_ID,mdaily.AC_Power ,mdaily.today_date FROM `Flexi_solar_MFM_list` AS mlist JOIN `Flexi_solar_MFM_daily` AS mdaily ON mdaily.plant_id = mlist.plant_id AND mdaily.M_id=mlist.M_ID  WHERE mlist.plant_id=%s AND mlist.MFM_Cumulative=1 AND mdaily.Today=%s;", siteID, todayD);
        String eventInvPlantDetQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_events where plant_id=%s and (device_type='Plant' or device_type='Inverter') and error_code IN (1000,2001) and (start_today=%s or end_today=%s or end_today is null) and category in ('1','3','5');", siteID, todayD, todayD);
        String invDevDetailQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverter_list where plant_id=%s", siteID);  
        String estimMonthQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_monthly_estimation_data where plant_id=%s;", siteID);  
        String wmsSensorEventQ=String.format("SELECT * FROM Flexi_solar_events WHERE plant_id =%s AND (start_today=%s or end_today=%s or end_today is null) AND error_code IN (8000,8001) group by device_name;",siteID, todayD, todayD);
//        String wmsCountQ=String.format("SELECT COUNT(id) FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s;",siteID);
        ResultSet plantInfoRs=stmt1.executeQuery(plantInfo);
        ResultSet wmsDailyRs=stmt2.executeQuery(wmsDailyQ);
        ResultSet mfmDailyRs=stmt3.executeQuery(mfmDailyQ);
        ResultSet rsEvent=stmt4.executeQuery(eventInvPlantDetQ);
        ResultSet invDevDetailRs=stmt5.executeQuery(invDevDetailQ);
        ResultSet estimMonthRs=stmt6.executeQuery(estimMonthQ);
        ResultSet wmsSensorEventRs=stmt7.executeQuery(wmsSensorEventQ);
//        ResultSet wmsCountRs=stmt8.executeQuery(wmsCountQ);
        
        String sunriseTimeAPI=null;
        String sunsetTimeAPI=null;
        String sunriseTimeStored=null;
        String sunsetTimeStored=null;
        String plantWakeupTStored=null;
        String plantSleepTStored=null;
        String sunriseTimeUpdate=null;
        String sunsetTimeUpdate=null;
//        int wmsCount=0;
        String device;
        String wmsID;
        String plantID;
        float degradation=0.0f;
        float dcCapacity=0.0f;
        float dcCapacityComp=0.0f;
        float acCapacity=0.0f;
        float lossFactor=0.0f;
        float modTempCoeff=0.0f;
        float estimtedPR=0.0f;
        Date DOC=null;
        float poaIrradiance;
        float poaIrrEve;
        float modTempAvg1;
        float modTempAvg2;
        float modTempAvgInst;
//        float modTempAvgInstEve;
        float ghiIrradiance;
        float ghiIrrEve;
        float lossIrrExtBD;
        float irrTilt1;
        float irrTilt1Avg;
        float irrTilt2;
        float irrTilt2Avg;
        float irrTiltAvgInst;
        float poaAvgInstEve;
        float irrTiltCAvgExcBD;
        float irrGlobal;
        float irrGlobalAvg;
//        float ghiAvgInstEve;
        float ambTemp;
        float ambTempAvg;
//        float ambTempAvgInstEve;
        float modTemp1;
        float modTemp2;
        float modTempExcBD;
//        float modTemp;
        float humidity;
        float humidityAvg;
        float windSpeed;
        float windSpeedAvg;
        float windDir;
        float windDirAvg;
        float roomTemp;
        float roomTempAvg;
        float acPower=0.0f;
        float ghiPoaGain;
        float valueHolderF=0.0f;
        String valueHolderS="";
        String dateID;
        String mongoDT;
        //check if data is calculated by considering wms sensor
//        boolean checkPoa;
//        boolean checkModtemp;
        //cummulative data
        String wmsMongoDT;
        int irrT1;
        int irrT2;
        int irrGl;
        int ambT;
        int modT1;
        int modT2;
        int hum;
        int windS;
        int windD;
        int roomT;
        int irrTCAvgExcBDC;
        int modTmpExcBDC;
        int irrT;
        int irrTC;
        float irrTCAvg;
        float irrTC1;
        float irrTC2;
        float modTempC1;
        float modTempC2;
        int modT;
        int modTC;
        float modTempCAvg;
        boolean checkForPlantCal;
        boolean checkForInvCal;
        boolean poaFail1;
        boolean poaFail2;
        boolean ghiFail1;
        boolean modTemFail1;
        boolean modTemFail2;
//        boolean ambTemFail1;
        String[] dev;
        float kWhExpectedC;
        float expectedPowIns;
        float lossEnergy;
        Set keyset;
        
        try{
            while(estimMonthRs.next()){
                if(estimMonthRs.getString("dateid").contentEquals(siteID+"-"+dateIDD)){
                    valueHolderS=estimMonthRs.getString("PR_guar");
                    if(valueHolderS!=null){
                        estimtedPR= Float.parseFloat(valueHolderS);
                    }
                    break;
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        while(mfmDailyRs.next()){
            valueHolderS=mfmDailyRs.getString("AC_Power");
            if(valueHolderS!=null){
                acPower=acPower+Float.parseFloat(valueHolderS);
            }
        }
        
//        if(wmsCountRs.next()){
//            wmsCount=Integer.parseInt(wmsCountRs.getString("COUNT(id)"));
//        }
        
        //sunrise sunset time data get
        while(wmsDailyRs.next()){
            if(wmsDailyRs.getString("wms_id").contentEquals("1")){
                try{
                    sunriseTimeStored=wmsDailyRs.getString("plant_sunrise_time");
                }
                catch (Exception ex) {
                    System.out.println("Catch wmsdaily wmsDailyRs.getString(\"plant_sunrise_time\")");
                }
                try{
                    sunsetTimeStored=wmsDailyRs.getString("plant_sunset_time");
                }
                catch (Exception ex) {
                    System.out.println("Catch wmsdaily wmsDailyRs.getString(\"plant_sunset_time\")");
                }
                try{
                    plantWakeupTStored=wmsDailyRs.getString("plant_wakeup_time");
                }
                catch (Exception ex) {
                    System.out.println("Catch wmsdaily wmsDailyRs.getString(\"plant_wakeup_time\")");
                }
                try{
                    plantSleepTStored=wmsDailyRs.getString("plant_sleep_time");
                }
                catch (Exception ex) {
                    System.out.println("Catch wmsdaily wmsDailyRs.getString(\"plant_sleep_time\")");
                }
                try{
                    sunriseTimeUpdate=wmsDailyRs.getString("sunrise_Time_updated");
                    if(sunriseTimeUpdate==null){
                        sunriseTimeUpdate="";
                    }
                }
                catch (Exception ex) {
                    System.out.println("Catch wmsdaily wmsDailyRs.getString(\"sunrise_Time_updated\")");
                }
                try{
                    sunsetTimeUpdate=wmsDailyRs.getString("sunset_Time_updated");
                    if(sunsetTimeUpdate==null){
                        sunsetTimeUpdate="";
                    }
                }
                catch (Exception ex) {
                    System.out.println("Catch wmsdaily wmsDailyRs.getString(\"sunset_Time_updated\")");
                }
                break;
            }
        }
        //for if logic in commulative data
        if(plantWakeupTStored==null){
            plantWakeupTStored="0000-00-00 00:00:00";
        }
        if(plantSleepTStored==null){
            plantSleepTStored="0000-00-00 00:00:00";
        }

        String plantLat=null;
        String plantLong=null;
        try{
            if(plantInfoRs.next()){
                valueHolderS=plantInfoRs.getString("plant_capacity");
                if(valueHolderS!=null){
                    acCapacity= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("plant_dc_capacity");
                if(valueHolderS!=null){
                    dcCapacity= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("loss_factor_plant_level");
                if(valueHolderS!=null){
                    lossFactor= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("module_temp_coefficient");
                if(valueHolderS!=null){
                    modTempCoeff= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("DOC");
                if(valueHolderS!=null){
                    DOC= DOCFormat.parse(valueHolderS);
                }
                try{
                    if(DOC!=null){
                        long daysPassed= (dateFormat.parse(currDT).getTime() - DOC.getTime())/(24 * 60 * 60 * 1000);
                        degradation= (float) ((daysPassed-365)*(0.7/36500));
                    }
                }
                catch (Exception ex) {
                    System.out.println("Catch wmsdaily degradation "+siteID);
                }
            }
        }
        catch (Exception ex) {
            System.out.println("Catch wmsdaily accapacity dc capacity lossfactor "+siteID);
        }
        
        try{
            if(sunriseTimeStored==null){
                plantLat=null;
                plantLong=null;
                plantInfoRs.beforeFirst();
                if(plantInfoRs.next()){
                    plantLat=plantInfoRs.getString("plant_location_latitude");
                    plantLong=plantInfoRs.getString("plant_location_logitude");
                }
                Location location = new Location(plantLat, plantLong);
                SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location,siteZone);
                sunriseTimeAPI = currD+" "+calculator.getOfficialSunriseForDate(Calendar.getInstance());
                sunsetTimeAPI = currD+" "+calculator.getOfficialSunsetForDate(Calendar.getInstance());
            }
        }
        catch (Exception ex) {
            System.out.println("Catch wmsdaily surise sunset api");
        }
        
        //latest data
        for(Document docLatest: docListMongoLatest){
            device=docLatest.getString("device");
            wmsID=null;
            plantID=null;
            poaIrradiance=0.0f;
            poaIrrEve=0.0f;
            ghiIrradiance=0.0f;
            ghiIrrEve=0.0f;
            lossIrrExtBD=0.0f;
            irrTilt1= 0.0f;
            irrTilt1Avg= 0.0f;
            irrTilt2= 0.0f;
            irrTilt2Avg= 0.0f;
            irrTiltAvgInst=0.0f;
            poaAvgInstEve=0.0f;
            irrTiltCAvgExcBD=0.0f;
            irrGlobal= 0.0f;
            irrGlobalAvg= 0.0f;
//            ghiAvgInstEve=0.0f;
            ambTemp= 0.0f;
            ambTempAvg= 0.0f;
//            ambTempAvgInstEve=0.0f;
            modTemp1= 0.0f;
            modTempAvg1= 0.0f;
            modTemp2= 0.0f;
            modTempAvg2= 0.0f;
            modTempAvgInst=0.0f;
            modTempExcBD= 0.0f;
            humidity= 0.0f;
            humidityAvg= 0.0f;
            windSpeed= 0.0f;
            windSpeedAvg= 0.0f;
            windDir= 0.0f;
            windDirAvg= 0.0f;
            roomTemp= 0.0f;
            roomTempAvg= 0.0f;
            ghiPoaGain=0.0f;
            kWhExpectedC=0.0f;
            expectedPowIns=0.0f;
            lossEnergy=0.0f;
            dateID=null;
            mongoDT=null;
            poaFail1=false;
            poaFail2=false;
            modTemFail1=false;
            modTemFail2=false;
            ghiFail1=false;
//            ambTemFail1=false;
//            checkPoa=false;
//            checkModtemp=false;

            try{
                dev=device.split("_");
                plantID=dev[0];
                wmsID=dev[2];
            }
            catch (Exception ex) {
                System.out.println("Catch device split");
            }

            while(wmsSensorEventRs.next()){
                if(wmsSensorEventRs.getString("device_id").contentEquals(wmsID)){
                    if(wmsSensorEventRs.getString("device_name").endsWith("POA-1")){
                        poaFail1=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("POA-2")){
                        poaFail2=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("Mod-Temp1")){
                        modTemFail1=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("Mod-Temp2")){
                        modTemFail2=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("GHI-1")){
                        ghiFail1=true;
                    }
//                    else if(wmsSensorEventRs.getString("device_name").endsWith("Amb-Temp")){
//                        ambTemFail1=true;
//                    }
                }
            }
            
            keyset=docLatest.keySet();
            
            try{
                if(keyset.contains("w23")){
                    mongoDT=docLatest.get("w23").toString();
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w23");
            }
            try{
                if(keyset.contains("w21")){
                    valueHolderF=Float.parseFloat(docLatest.get("w21").toString());
                    if(!Float.isNaN(valueHolderF)){
                        irrTilt1=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w21");
            }
            try{
                if(keyset.contains("w22")){
                    valueHolderF=Float.parseFloat(docLatest.get("w22").toString());
                    if(!Float.isNaN(valueHolderF)){
                        irrTilt2=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w22");
            }
            
            //POA avg on latest 
            irrT=0;
            if(irrTilt1>0){
                irrTiltAvgInst=irrTilt1;
                irrT++;
            }
            if(irrTilt2>0){
                irrTiltAvgInst=irrTiltAvgInst+irrTilt2;
                irrT++; 
            }
            if(irrT>1){
                irrTiltAvgInst=irrTiltAvgInst/irrT;
            }
            //poa avg on inst considering event
//            irrT=0;
//            if(!poaFail1){
//                if(irrTilt1>0){
//                    poaAvgInstEve=irrTilt1;
//                    irrT++;
//                }
//            }
//            if(!poaFail2){
//                if(irrTilt2>0){
//                    poaAvgInstEve=poaAvgInstEve+irrTilt2;
//                    irrT++; 
//                }
//            }
//            if(irrT>1){
//                poaAvgInstEve=poaAvgInstEve/irrT;
//            }
//            if(irrTiltAvgInst==0.0){
//                irrTiltAvgInst=irrTilt1;
//                checkPoa=true;
//            }
            try{
                if(keyset.contains("w32")){
                    valueHolderF=Float.parseFloat(docLatest.get("w32").toString());
                    if(!Float.isNaN(valueHolderF)){
                        irrGlobal=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w32");
            }
//            if(!ghiFail1){
//                ghiAvgInstEve=irrGlobal;
//            }
            try{
                if(keyset.contains("w13")){
                    valueHolderF=Float.parseFloat(docLatest.get("w13").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ambTemp=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w13");
            }
//            if(!ambTemFail1){
//                ambTempAvgInstEve=ambTemp;
//            }
            try{
                if(keyset.contains("w10")){
                    valueHolderF=Float.parseFloat(docLatest.get("w10").toString());
                    if(!Float.isNaN(valueHolderF)){
                        modTemp1=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w10");
            }
            try{
                if(keyset.contains("w14")){
                    valueHolderF=Float.parseFloat(docLatest.get("w14").toString());
                    if(!Float.isNaN(valueHolderF)){
                        modTemp2=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w14");
            }
            
            //mod temp avg on latest 
            modT=0;
            if(modTemp1>0){
                modTempAvgInst=modTemp1;
                modT++;
            }
            if(modTemp2>0){
               modTempAvgInst=modTempAvgInst+modTemp2;
                modT++; 
            }
            if(modT>1){
                modTempAvgInst=modTempAvgInst/modT;
            }
            //mod temp avg on latest considering event
//            modT=0;
//            if(!modTemFail1){
//                if(modTemp1>0){
//                    modTempAvgInstEve=modTemp1;
//                    modT++;
//                }
//            }
//            if(!modTemFail1){
//                if(modTemp2>0){
//                   modTempAvgInstEve=modTempAvgInstEve+modTemp2;
//                    modT++; 
//                }
//            }
//            if(modT>1){
//                modTempAvgInstEve=modTempAvgInstEve/modT;
//            }
//            if(wmsCount<=1 && modTempAvgInst==0.0){
//                modTempAvgInst=modTemp1;
//                checkModtemp=true;
//            }
            
            try{
                if(keyset.contains("w16")){
                    valueHolderF=Float.parseFloat(docLatest.get("w16").toString());
                    if(!Float.isNaN(valueHolderF)){
                        humidity=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w16");
            }
            try{
                if(keyset.contains("w12")){
                    valueHolderF=Float.parseFloat(docLatest.get("w12").toString());
                    if(!Float.isNaN(valueHolderF)){
                        windSpeed=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w12");
            }
            try{
                if(keyset.contains("w11")){
                    valueHolderF=Float.parseFloat(docLatest.get("w11").toString());
                    if(!Float.isNaN(valueHolderF)){
                        windDir=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w11");
            }
            try{
                if(keyset.contains("w35")){
                    valueHolderF=Float.parseFloat(docLatest.get("w35").toString());
                    if(!Float.isNaN(valueHolderF)){
                        roomTemp=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w35");
            }
            
            try{
                expectedPowIns=dcCapacity*(1-degradation)*(1-lossFactor)*(1+(modTempCoeff*((modTempAvgInst+(irrTiltAvgInst/1000)*3)-25)))*(irrTiltAvgInst/1000);                 
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily w35");
            }

            //cummulative data
            wmsMongoDT="0000-00-00 00:00:00";
            irrT1=0;
            irrT2=0;
            irrGl=0;
            ambT=0;
            modT1=0;
            modT2=0;
            hum=0;
            windS=0;
            windD=0;
            roomT=0;
            irrTCAvgExcBDC=0;
            modTmpExcBDC=0;
            irrTC=0;
            irrTCAvg=0.0f;
            irrTC1=0.0f;
            irrTC2=0.0f;
            modTempC1=0.0f;
            modTempC2=0.0f;
            modTC=0;
            modTempCAvg=0.0f;
            checkForPlantCal=true;
            checkForInvCal=false;
            
            docListMongo=getdata.mongoDevGteData1(device, todayStartDT, mongocollection, conn, siteID);
            for(Document doc:docListMongo){
                try {
                   keyset=doc.keySet();
                   if(sunriseTimeStored!=null && dateFormat.parse(sunriseTimeStored).compareTo(dateFormat.parse(doc.get("w23").toString()))<=0){
                        if(sunsetTimeStored!=null || dateFormat.parse(sunsetTimeStored).compareTo(dateFormat.parse(doc.get("w23").toString()))>=0){
                            irrTC1=0.0f;
                            irrTC2=0.0f;
                            modTempC1=0.0f;
                            modTempC2=0.0f;
                            try{
                                if(keyset.contains("w21")){
                                    valueHolderF=Float.parseFloat(doc.get("w21").toString());
                                    if(!Float.isNaN(valueHolderF) && valueHolderF<2500 && valueHolderF>0){
                                        irrTC1=valueHolderF;
                                        poaIrradiance=poaIrradiance+(irrTC1/60000);
                                        irrTilt1Avg=irrTilt1Avg+valueHolderF;
                                        irrT1++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w21cumm");
                            }
                            
                            try{
                                if(keyset.contains("w22")){
                                    valueHolderF=Float.parseFloat(doc.get("w22").toString());
                                    if(!Float.isNaN(valueHolderF) && valueHolderF<2500 && valueHolderF>0){
                                        irrTC2=valueHolderF;
                                        poaIrradiance=poaIrradiance+(irrTC2/60000);
                                        irrTilt2Avg=irrTilt2Avg+valueHolderF;
                                        irrT2++;
                                    }
                                } 
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w22cumm");
                            }
                            
                            //POA avg on inst for sunrise.sunset and wakeup/sleep and lossirr
                            try{
                                irrTC=0;
                                irrTCAvg=0.0f;
                                if(!poaFail1){
                                    if(irrTC1>0.0){
                                        irrTCAvg=irrTC1;
                                        irrTC++;
                                    }
                                }
                                if(!poaFail2){
                                    if(irrTC2>0.0){
                                        irrTCAvg=irrTCAvg+irrTC2;
                                        irrTC++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily irrTCAvg cumm");
                            }

                            if(irrTC>1){
                                irrTCAvg=irrTCAvg/irrTC;
                            }

                            if(!Float.isNaN(irrTCAvg)){
                                poaIrrEve=poaIrrEve+(irrTCAvg/60000);
                            }
                            
                            try{
                                if(keyset.contains("w32")){
                                    valueHolderF=Float.parseFloat(doc.get("w32").toString());
                                    if(!Float.isNaN(valueHolderF) && valueHolderF<2500 && valueHolderF>0){
                                        irrGlobalAvg=irrGlobal+valueHolderF;
                                        irrGl++;
                                        ghiIrradiance=ghiIrradiance+(valueHolderF/60000);
                                        if(!ghiFail1){
                                            ghiIrrEve=ghiIrrEve+(valueHolderF/60000);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w32 cumm");
                            }

                            try{
                                if(keyset.contains("w13")){
                                    valueHolderF=Float.parseFloat(doc.get("w13").toString());
                                    if(!Float.isNaN(valueHolderF)){
                                        ambTempAvg=ambTempAvg+valueHolderF;
                                        ambT++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w13 cumm");
                            }

                            try{
                                if(keyset.contains("w10")){
                                    valueHolderF=Float.parseFloat(doc.get("w10").toString());
                                    if(!Float.isNaN(valueHolderF)){
                                        modTempC1=valueHolderF;
                                        modTempAvg1=modTempAvg1+valueHolderF;
                                        modT1++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w10 cumm");
                            }
                            try{
                                if(keyset.contains("w14")){
                                    valueHolderF=Float.parseFloat(doc.get("w14").toString());
                                    if(!Float.isNaN(valueHolderF)){
                                        modTempC2=valueHolderF;
                                        modTempAvg2=modTempAvg2+valueHolderF;
                                        modT2++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w14 cumm");
                            }
                            
                            try{
                                modTC=0;
                                modTempCAvg=0.0f;
                                if(!modTemFail1){
                                    if(modTempC1>0){
                                        modTempCAvg=modTempC1;
                                        modTC++;
                                    }
                                }
                                if(!modTemFail2){
                                    if(modTempC2>0){
                                        modTempCAvg=modTempCAvg+modTempC2;
                                        modTC++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily modtempcavg cumm");
                            }

                            if(modTC>1){
                                modTempCAvg=modTempCAvg/modTC;
                            }

                            try{
                                if(keyset.contains("w16")){
                                    valueHolderF=Float.parseFloat(doc.get("w16").toString());
                                    if(!Float.isNaN(valueHolderF)){
                                        humidityAvg=humidityAvg+valueHolderF;
                                        hum++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w16 cumm");
                            }

                            try{
                                if(keyset.contains("w12")){
                                    valueHolderF=Float.parseFloat(doc.get("w12").toString());
                                    if(!Float.isNaN(valueHolderF)){
                                        windSpeedAvg=windSpeedAvg+valueHolderF;
                                        windS++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w12 cumm");
                            }

                            try{
                                if(keyset.contains("w11")){
                                    valueHolderF=Float.parseFloat(doc.get("w11").toString());
                                    if(!Float.isNaN(valueHolderF)){
                                        windDirAvg=windDirAvg+valueHolderF;
                                        windD++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w11 cumm");
                            }

                            try{
                                if(keyset.contains("w35")){
                                    valueHolderF=Float.parseFloat(doc.get("w35").toString());
                                    if(!Float.isNaN(valueHolderF)){
                                        roomTempAvg=roomTempAvg+valueHolderF;
                                        roomT++;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w35 cumm");
                            }

                            try{
                                if(keyset.contains("w23")){
                                    wmsMongoDT=doc.get("w23").toString();
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily w23 cumm");
                            }

                            try{
                                checkForPlantCal=true;
                                checkForInvCal=false;
                                dcCapacityComp=0.0f;
                                rsEvent.beforeFirst();
                                while(rsEvent.next()){
                                    if(rsEvent.getString("device_type").contentEquals("Plant")){
                                        if((dateFormat.parse(wmsMongoDT)).compareTo(dateFormat.parse(rsEvent.getString("start_date")))>0 && rsEvent.getString("end_date")==null){
                                            checkForPlantCal=false;
                                            break;
                                        }
                                        else if(((dateFormat.parse(wmsMongoDT)).compareTo(dateFormat.parse(rsEvent.getString("start_date"))))>0 && ((dateFormat.parse(wmsMongoDT)).compareTo(dateFormat.parse(rsEvent.getString("end_date"))))<0){
                                            checkForPlantCal=false;
                                            break;
                                        }
                                    }
                                    if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                        if((dateFormat.parse(wmsMongoDT)).compareTo(dateFormat.parse(rsEvent.getString("start_date")))>0 && rsEvent.getString("end_date")==null){
                                            invDevDetailRs.beforeFirst();
                                            while(invDevDetailRs.next()){
                                                if((rsEvent.getString("device_id")).contentEquals(invDevDetailRs.getString("inverter_id"))){
                                                    dcCapacityComp=dcCapacity-Float.parseFloat(invDevDetailRs.getString("inverter_dc_capacity"));
                                                    checkForInvCal=true;
                                                }
                                            }
                                        }
                                        else if((dateFormat.parse(wmsMongoDT)).compareTo(dateFormat.parse(rsEvent.getString("start_date")))>0 && (dateFormat.parse(wmsMongoDT)).compareTo(dateFormat.parse(rsEvent.getString("end_date")))<0){
                                            invDevDetailRs.beforeFirst();
                                            while(invDevDetailRs.next()){
                                                if((rsEvent.getString("device_id")).contentEquals(invDevDetailRs.getString("inverter_id"))){
                                                    dcCapacityComp=dcCapacity-Float.parseFloat(invDevDetailRs.getString("inverter_dc_capacity"));
                                                    checkForInvCal=true;
                                                } 
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex) {
                                System.out.println("catch wmsdaily plant eventcheck cumm "+siteID);
                            }

                            //for exculding bd calculation
                            if(checkForPlantCal){
                                try{
                                    irrTiltCAvgExcBD=irrTiltCAvgExcBD+irrTCAvg;
                                    irrTCAvgExcBDC++;
                                }
                                catch (Exception ex) {
                                    System.out.println("catch wmsdaily irrTiltCAvgExcBD cumm");
                                }
                                try{
                                    modTempExcBD=modTempExcBD+modTempCAvg;
                                    modTmpExcBDC++;
                                }
                                catch (Exception ex) {
                                    System.out.println("catch wmsdaily modTempExcBD cumm");
                                }
                            }

                            if(!checkForPlantCal){
                                lossIrrExtBD=lossIrrExtBD+(irrTCAvg/60000);
                            }
                            
                            if(modTempCAvg>0 && irrTCAvg>0){
                                float expectedPow=0;
                                if(checkForPlantCal && !checkForInvCal){
                                    try{
                                        expectedPow=dcCapacity*(1-degradation)*(1-lossFactor)*(1+(modTempCoeff*((modTempCAvg+(irrTCAvg/1000)*3)-25)))*(irrTCAvg/1000);
                                        if(acCapacity<expectedPow){
                                            expectedPow=acCapacity;
                                        }
                                        kWhExpectedC= kWhExpectedC+(expectedPow/60);
                                    }
                                    catch (Exception ex) {
                                        System.out.println("catch wmsdaily checkForPlantCal && !checkForInvCal cumm");
                                    }
                                }
                                else if(checkForInvCal){
                                    try{
                                        expectedPow=dcCapacityComp*(1-degradation)*(1-lossFactor)*(1+(modTempCoeff*((modTempCAvg+(irrTCAvg/1000)*3)-25)))*(irrTCAvg/1000);
                                        if(acCapacity<expectedPow){
                                            expectedPow=acCapacity;
                                        }
                                        kWhExpectedC= kWhExpectedC+(expectedPow/60);
                                    }
                                    catch (Exception ex) {
                                        System.out.println("catch wmsdaily  !checkForInvCal cumm");
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("Catch wmsdaily cummulatiive values whole "+siteID);
                }
                
            }
            
            if(irrT1>1){
                irrTilt1Avg=irrTilt1Avg/irrT1;
            }
            if(irrT2>1){
                irrTilt2Avg=irrTilt2Avg/irrT2;
            }
            if(irrGl>1){
                irrGlobalAvg=irrGlobalAvg/irrGl;
            }
            if(ambT>1){
                ambTempAvg=ambTempAvg/ambT;
            }
            if(modT1>1){
             modTempAvg1=modTempAvg1/modT1;
            }
            if(modT2>1){
                modTempAvg2=modTempAvg2/modT2;
            }
            if(hum>1){
                humidityAvg=humidityAvg/hum;
            }
            if(windS>1){
                windSpeedAvg=windSpeedAvg/windS;
            }
            if(windD>1){
                windDirAvg=windDirAvg/windD;
            }
            if(roomT>1){
                roomTempAvg=roomTempAvg/roomT;
            }
            if(irrTCAvgExcBDC>1){
                irrTiltCAvgExcBD=irrTiltCAvgExcBD/irrTCAvgExcBDC;
            }
            if(modTmpExcBDC>1){
                modTempExcBD=modTempExcBD/modTmpExcBDC;
            }
            if(irrGlobal>0.0){
                ghiPoaGain=((irrTiltAvgInst-irrGlobal)/irrGlobal)*100;
            }
            
            try{
                lossEnergy=getdata.lossEnergy(estimtedPR, siteID, siteZone, device, docListMongo, mongocollection, conn);
            }
            catch (Exception ex) {
                System.out.println("catch wmsdaily lossenergy cal "+siteID);
            }

            dateID=plantID+"-"+wmsID+"-"+todayD;
            boolean match=false;
            try{
                wmsDailyRs.beforeFirst();
                while(wmsDailyRs.next()){
                    if((wmsDailyRs.getString("dateid")).contentEquals(dateID)){
                        String ID=wmsDailyRs.getString("id");
                        sqlupdateandquery.sqlWmsDailyUpdate(wmsDailyRs,conn,ID,currDT,mongoDT,poaIrradiance,lossIrrExtBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTempAvg1,modTemp2,modTempAvg2,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,sunriseTimeAPI,sunsetTimeAPI,sunriseTimeStored,sunsetTimeStored,sunriseTimeUpdate,sunsetTimeUpdate,plantWakeupTStored,plantSleepTStored,acPower,kWhExpectedC,expectedPowIns,lossEnergy);
                        match=true;
                        break;
                    }
                }
                if(!match){
                    sqlupdateandquery.sqlWmsDailyEntry(wmsDailyRs,conn,plantID,wmsID,currDT,mongoDT,todayD,dateID,poaIrradiance,lossIrrExtBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTempAvg1,modTemp2,modTempAvg2,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,sunriseTimeAPI,sunsetTimeAPI,sunriseTimeStored,sunsetTimeStored,sunriseTimeUpdate,sunsetTimeUpdate,plantWakeupTStored,plantSleepTStored,acPower,kWhExpectedC,expectedPowIns,lossEnergy);
                }
            }
            catch (Exception ex) {
                System.out.println("Catch wmsdaily update entry query");
            }

        }
        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
            stmt4.close();
            stmt5.close();
            stmt6.close();
            stmt7.close();
//            stmt8.close();
        } catch (Exception ex) {
            System.out.println("Catch stmt close");
        }

    }
    
    
    public void mfmDaily(List<Document> docListMongoLatest, String siteID, String siteZone, Connection conn, MongoCollection mongocollection, Instant todayStartDT) throws SQLException{
        List<Document> docListMongo=new ArrayList<>();
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String currDT=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String yesterdayD=zoneddatetime.minusDays(1).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        String mfmDailyQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_daily where plant_id=%s and (Today='%s' or Today='%s');", siteID, todayD, yesterdayD);  
        String plantInfoQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_plant_info where id=%s;", siteID);  
        String wmsDailyQ=String.format("SELECT wlist.wms_id,wdaily.poa_irradiance,wdaily.today_date FROM `Flexi_solar_wms_list` AS wlist JOIN `Flexi_solar_wms_daily` AS wdaily ON wdaily.plant_id = wlist.plant_id AND wdaily.wms_id=wlist.wms_id WHERE wlist.plant_id=%s AND wlist.WMS_Cumulative=1 AND wdaily.Today=%s;", siteID, todayD);  
        ResultSet mfmDailyRs=stmt1.executeQuery(mfmDailyQ);
        ResultSet plantInfoRs=stmt2.executeQuery(plantInfoQ);
        ResultSet wmsDailyRs=stmt3.executeQuery(wmsDailyQ);
        
        String device;
        String mfmID;
        String plantID;

        float dcCapacity=0.0f;
        float irr_avg=0.0f;
        float kwhTotalExpYesterday;
        float kwhTotalImpYesterday;
        float kwhTotalExp;
        float kwhTotalImp;
        float kwhTodayExp;
        float kwhTodayImp;
        float ACPower;
        float MaxACPower;
        float ReacPower;
        float mfmPR;
        float valueHolderF=0.0f;
        String valueHolderS="";
        
        String updateTime;
        String mongoDate;
        String todayDate;
        String dateID;
        Set keyset;
        
        plantInfoRs.beforeFirst();
        while(plantInfoRs.next()){
            valueHolderS=plantInfoRs.getString("plant_dc_capacity");
            if(valueHolderS!=null){
                dcCapacity= Float.parseFloat(valueHolderS);
            }
        }
        
        float irrTot=0.0f;
        int irrC=0;
        wmsDailyRs.beforeFirst();
        while(wmsDailyRs.next()){
            valueHolderS=wmsDailyRs.getString("poa_irradiance");
            if(valueHolderS!=null){
                irrTot=irrTot+Float.parseFloat(valueHolderS);
                irrC++;
            }
        }
        if(irrC>0){
            irr_avg=irrTot/irrC;
        }
        
        //latest data
        for(Document docLatest: docListMongoLatest){
            device=docLatest.getString("device");
            mfmID=null;
            plantID=null;
            
            kwhTotalExpYesterday=0.0f;
            kwhTotalImpYesterday=0.0f;
            kwhTotalExp=0.0f;
            kwhTotalImp=0.0f;
            kwhTodayExp=0.0f;
            kwhTodayImp=0.0f;
            ACPower=0.0f;
            MaxACPower=0.0f;
            ReacPower=0.0f;
            mfmPR=0.0f;
            
            mongoDate=null;
            dateID=null;

            
            try{
                String[] dev=device.split("_");
                plantID=dev[0];
                mfmID=dev[2];
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }

            dateID=plantID+"-"+mfmID+"-"+todayD;
            //kwhTotalYesterday
            mfmDailyRs.beforeFirst();
            while(mfmDailyRs.next()){
                if((mfmDailyRs.getString("dateid")).contentEquals(plantID+"-"+mfmID+"-"+yesterdayD)){
                    valueHolderS=mfmDailyRs.getString("Energy_Export_total");
                    if(valueHolderS!=null){
                        kwhTotalExpYesterday= Float.parseFloat(valueHolderS);
                    }
                    valueHolderS=mfmDailyRs.getString("Energy_import_total");
                    if(valueHolderS!=null){
                        kwhTotalImpYesterday= Float.parseFloat(valueHolderS);
                    }
                }
                if((mfmDailyRs.getString("dateid")).contentEquals(dateID)){
                    valueHolderS=mfmDailyRs.getString("Max_AC_Power");
                    if(valueHolderS!=null){
                        MaxACPower= Float.parseFloat(valueHolderS);
                    }
                }
            }
            
            keyset=docLatest.keySet();
            try{
                if(keyset.contains("m66")){
                    valueHolderF=Float.parseFloat(docLatest.get("m66").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhTotalExp=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("m65")){
                    valueHolderF=Float.parseFloat(docLatest.get("m65").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhTotalImp=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("m63")){
                    valueHolderF=Float.parseFloat(docLatest.get("m63").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhTodayExp=valueHolderF;
                    }
                }
                else if((docLatest.keySet()).contains("m66")){
                    valueHolderF=Float.parseFloat(docLatest.get("m66").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhTodayExp=valueHolderF-kwhTotalExpYesterday;
                    }
                }
                if(kwhTodayExp<=0 && (docLatest.keySet()).contains("m66")){
                    valueHolderF=Float.parseFloat(docLatest.get("m66").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhTodayExp=valueHolderF-kwhTotalExpYesterday;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("m92")){
                    valueHolderF=Float.parseFloat(docLatest.get("m92").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhTodayImp=valueHolderF;
                    }
                }
                else if((docLatest.keySet()).contains("m65")){
                    valueHolderF=Float.parseFloat(docLatest.get("m65").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhTodayImp=valueHolderF-kwhTotalImpYesterday;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("m32")){
                    valueHolderF=Float.parseFloat(docLatest.get("m32").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ACPower=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            if(MaxACPower<ACPower){
                MaxACPower=ACPower;
            }
            try{
                if(keyset.contains("m37")){
                    valueHolderF=Float.parseFloat(docLatest.get("m37").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ReacPower=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("m64")){
                    mongoDate=(docLatest.get("m64").toString());
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(irr_avg>0.0 && dcCapacity>0.0){
                    mfmPR=(kwhTodayExp/(irr_avg*dcCapacity))*100;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            boolean match=false; 
            try{
                mfmDailyRs.beforeFirst();
                while(mfmDailyRs.next()){
                    if((mfmDailyRs.getString("dateid")).contentEquals(dateID)){
                        String ID=mfmDailyRs.getString("id");
                        sqlupdateandquery.sqlMfmDailyUpdate(conn,ID,currDT,mongoDate,kwhTodayExp,kwhTodayImp,kwhTotalExp,kwhTotalImp,ACPower,MaxACPower,ReacPower,mfmPR);
                        match=true;
                        break;
                    }
                }
                if(!match){
                    sqlupdateandquery.sqlMfmDailyEntry(conn,plantID,mfmID,currDT,todayD,dateID,mongoDate,kwhTodayExp,kwhTodayImp,kwhTotalExp,kwhTotalImp,ACPower,MaxACPower,ReacPower,mfmPR);
                }
            }
            catch (Exception ex) {
                System.out.println("Catch wmsdaily update entry query");
            }    
            
        }
        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
    
    public void inverterDaily(List<Document> docListMongoLatest, String siteID, String siteZone, Connection conn, MongoCollection mongocollection, Instant todayStartDT) throws SQLException, ParseException{
        List<Document> docListMongo=new ArrayList<>();
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String currDT=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String yesterdayD=zoneddatetime.minusDays(1).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        Statement stmt4= conn.createStatement();

        String invDailyQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverters_daily where plant_id=%s and (Today='%s' or Today='%s');", siteID, todayD, yesterdayD);  
        String invDevDetailQ=String.format("SELECT inverter_id, inverter_dc_capacity FROM swlflexi_CMSswlawsReMACS.Flexi_solar_inverter_list where plant_id=%s", siteID);  
        String wmsDailyQ=String.format("SELECT wdaily.wms_id,wdaily.poa_irradiance,wdaily.today_date,wdaily.plant_sunrise_time,wdaily.plant_sunset_time,wdaily.sunrise_Time_updated,wdaily.sunset_Time_updated FROM `Flexi_solar_wms_list` AS wlist JOIN `Flexi_solar_wms_daily` AS wdaily ON wdaily.plant_id = wlist.plant_id AND wdaily.wms_id=wlist.wms_id WHERE wlist.plant_id=%s AND wlist.WMS_Cumulative=1 AND wdaily.Today=%s;", siteID, todayD);  
        String eventInvPlantDetQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_events where plant_id=%s and (device_type='Plant' or device_type='Inverter') and category not in (1,3,5) and (error_code=1000 or error_code=2001) and (start_today=%s or end_today=%s or end_today is null);", siteID, todayD, todayD);
        
        ResultSet invDailyRs=stmt1.executeQuery(invDailyQ);
        ResultSet invDetailRs=stmt2.executeQuery(invDevDetailQ);
        ResultSet wmsDailyRs=stmt3.executeQuery(wmsDailyQ);
        ResultSet rsEvent=stmt4.executeQuery(eventInvPlantDetQ);

        String device;
        float ACCurr_L1;
        float ACCurr_L2;
        float ACCurr_L3;
        float ACVolt_L1;
        float ACVolt_L2;
        float ACVolt_L3;
        float DCCurr;
        float DCVolt;
        float DCPow;
        float ACPow;
        int invStatus;
        String invID;
        String plantID;
        float invTemp;
        float kwhTotal;
        float kwhTotalYesterday;
        float kwhToday;
        float kwhTodayMinCl;
        float dcEnergy;
        float invPR;
        float invDCCapacity;
        float acCumm;
        float dcCumm;
        float invAvaRatio;;
        Date sunriseTime=null;
        Date sunsetTime=null;
        long totMin;
        long bdMin;
        long excMin;
        Set keyset;
        String mongoDate;
        String dateID;
        float valueHolderF=0.0f;
        String valueHolderS="";
        
        //irrediation
        float irr_avg=0.0f;
        float irrTot=0.0f;
        int irrC=0;
        
        wmsDailyRs.beforeFirst();
        while(wmsDailyRs.next()){
            try{
                if(wmsDailyRs.getString("wms_id").contentEquals("1") && wmsDailyRs.getString("sunrise_Time_updated").contentEquals("1")){
                    try {
                        sunriseTime=dateFormat.parse(wmsDailyRs.getString("plant_sunrise_time"));
                    } catch (Exception ex) {
                        System.out.println("Catch inverter daily 1");
                    }
                }
            }
            catch (Exception ex) {    
            }
            
            try{
                if(wmsDailyRs.getString("wms_id").contentEquals("1") && wmsDailyRs.getString("sunset_Time_updated").contentEquals("0")){
                    try {
                        sunsetTime=dateFormat.parse(wmsDailyRs.getString("plant_sunset_time"));
                    } catch (Exception ex) {
                        System.out.println("Catch inverter daily 2");
                    }
                }
            }
            catch (Exception ex) {
            }
            
            try{
                valueHolderF=Float.parseFloat(wmsDailyRs.getString("poa_irradiance"));
                if(!Float.isNaN(valueHolderF)){
                    irrTot=irrTot+valueHolderF;
                    irrC++;
                }
            }
            catch (Exception ex) {
                System.out.println("Catch inverter daily 3");
            }
        }

        if(irrC>0){
            irr_avg=irrTot/irrC;
        }
        
        //latest data
        for(Document docLatest:docListMongoLatest){
            device=docLatest.getString("device");
            ACCurr_L1=0.0f;
            ACCurr_L2=0.0f;
            ACCurr_L3=0.0f;
            ACVolt_L1=0.0f;
            ACVolt_L2=0.0f;
            ACVolt_L3=0.0f;
            DCCurr=0.0f;
            DCVolt=0.0f;
            DCPow=0.0f;
            ACPow=0.0f;
            invStatus=2;
            invID=null;
            plantID=null;
            invTemp=0.0f;
            kwhTotal=0.0f;
            kwhTotalYesterday=0.0f;
            kwhToday=0.0f;
            kwhTodayMinCl=0.0f;
            dcEnergy=0.0f;
            invPR=0.0f;
            invDCCapacity=0.0f;
            invAvaRatio=0.0f;
            totMin=0;
            bdMin=0;
            excMin=0;
            
            mongoDate=null;
            dateID=null;

            
            try{
                String[] dev=device.split("_");
                plantID=dev[0];
                invID=dev[2];
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            //kwhTotalYesterday
            invDailyRs.beforeFirst();
            while(invDailyRs.next()){
                if((invDailyRs.getString("dateid")).contentEquals(plantID+"-"+invID+"-"+yesterdayD)){
                    valueHolderS=invDailyRs.getString("kwh_till_date");
                    if(valueHolderS!=null){
                        kwhTotalYesterday=Float.parseFloat(valueHolderS);
                    }
                    break;
                }
            }
            //invDCCapacity
            invDetailRs.beforeFirst();
            while(invDetailRs.next()){
                if(invDetailRs.getString("inverter_id").contentEquals(invID)){
                    valueHolderS=invDetailRs.getString("inverter_dc_capacity");
                    if(valueHolderS!=null){
                        invDCCapacity=Float.parseFloat(valueHolderS);
                    }
                    break;
                }
            }
            
            //availability calculation
            try {
                if(sunriseTime==null){
                    invAvaRatio=0.0f;
                }
                else if(sunriseTime!=null && sunsetTime==null){
                    totMin=((dateFormat.parse(currDT)).getTime()-sunriseTime.getTime())/(60*1000);
                }
                else if(sunriseTime!=null && sunsetTime!=null){
                    totMin=((sunsetTime).getTime()-sunriseTime.getTime())/(60*1000);
                }
            }
            catch (Exception ex) {
                System.out.println("catch inverteravaratio");
            }
            
            rsEvent.beforeFirst();
            while(rsEvent.next()){
                if(sunriseTime!=null){
                    if(rsEvent.getString("device_id").contentEquals(invID)){
                        //add in breakdown time
                        try{
                            if(sunsetTime==null && rsEvent.getString("end_date")==null){
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0){
                                    bdMin=bdMin+((dateFormat.parse(currDT).getTime()-(dateFormat.parse(rsEvent.getString("start_date")).getTime()))/(60*1000));
                                }
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<0){
                                    bdMin=bdMin+((dateFormat.parse(currDT).getTime()-(sunriseTime).getTime())/(60*1000));
                                }
                            }
                            else if(sunsetTime==null && rsEvent.getString("end_date")!=null){
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0){
                                    bdMin=bdMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                                }
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunriseTime)>=0){
                                    bdMin=bdMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(sunriseTime).getTime())/(60*1000));
                                }
                            }
                            else if(sunsetTime!=null && rsEvent.getString("end_date")==null){
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunsetTime)<=0){
                                    bdMin=bdMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                                }
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0){
                                    bdMin=bdMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                                }
                            }
                            else if(sunsetTime!=null && rsEvent.getString("end_date")!=null){
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)<=0){
                                    bdMin=bdMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                                }
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunsetTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)>=0){
                                    bdMin=bdMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                                }
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunriseTime)>=0){
                                    bdMin=bdMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-sunriseTime.getTime())/(60*1000));
                                }
                                if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)>=0){
                                    bdMin=bdMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                                }
                            }
                        }
                        catch (Exception ex) {
                            System.out.println("catch bdmino");
                        }
                    }
                }
            }
            
            if(totMin>0.0){
                invAvaRatio=((float)(totMin-bdMin)/totMin);
            }

            docListMongo=getdata.mongoDevGteData1(device, todayStartDT, mongocollection, conn, siteID);
            //today dc kwh energy
            acCumm=0.0f;
            dcCumm=0.0f;
            for(Document doc:docListMongo){
                keyset=doc.keySet();
                try{
                    if(keyset.contains("i15")){
                        valueHolderF=Float.parseFloat(doc.get("i15").toString());
                        if(!Float.isNaN(valueHolderF)){
                            acCumm=acCumm+valueHolderF;
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                try{
                    if(keyset.contains("i21")){
                        valueHolderF=Float.parseFloat(doc.get("i21").toString());
                        if(!Float.isNaN(valueHolderF)){
                            dcCumm=dcCumm+valueHolderF;
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                try{
                    if(keyset.contains("i40")){
                        valueHolderF=Float.parseFloat(doc.get("i40").toString());
                        if(!Float.isNaN(valueHolderF)){
                            dcCumm=dcCumm+valueHolderF;
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            kwhTodayMinCl=acCumm/60;
            dcEnergy=dcCumm/60;

            //other tag data
            keyset=docLatest.keySet();

            try{
                if(keyset.contains("i12")){
                    valueHolderF=Float.parseFloat(docLatest.get("i12").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ACCurr_L1=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i13")){
                    valueHolderF=Float.parseFloat(docLatest.get("i13").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ACCurr_L2=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            try{
                if(keyset.contains("i14")){
                    valueHolderF=Float.parseFloat(docLatest.get("i14").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ACCurr_L3=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i9")){
                    valueHolderF=Float.parseFloat(docLatest.get("i9").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ACVolt_L1=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            try{
                if(keyset.contains("i10")){
                    valueHolderF=Float.parseFloat(docLatest.get("i10").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ACVolt_L2=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i11")){
                    valueHolderF=Float.parseFloat(docLatest.get("i11").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ACVolt_L3=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i20")){
                    valueHolderF=Float.parseFloat(docLatest.get("i20").toString());
                    if(!Float.isNaN(valueHolderF)){
                        DCCurr=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i29")){
                    valueHolderF=Float.parseFloat(docLatest.get("i29").toString());
                    if(!Float.isNaN(valueHolderF)){
                        DCVolt=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i33")){
                    valueHolderF=Float.parseFloat(docLatest.get("i33").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhTotal=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i21")){
                    valueHolderF=Float.parseFloat(docLatest.get("i21").toString());
                    if(!Float.isNaN(valueHolderF)){
                        DCPow=valueHolderF;
                    }
                }
                if(keyset.contains("i40")){
                    valueHolderF=Float.parseFloat(docLatest.get("i40").toString());
                    if(!Float.isNaN(valueHolderF)){
                        DCPow=DCPow+valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i15")){
                    valueHolderF=Float.parseFloat(docLatest.get("i15").toString());
                    if(!Float.isNaN(valueHolderF)){
                        ACPow=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(ACPow>0){
                    //initialized with 2
                    invStatus=4;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("i22")){
                    valueHolderF=Float.parseFloat(docLatest.get("i22").toString());
                    if(!Float.isNaN(valueHolderF)){
                        invTemp=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            try{
                if(keyset.contains("i31")){
                    valueHolderF=Float.parseFloat(docLatest.get("i31").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhToday=valueHolderF;
                    }
                }
                else if((docLatest.keySet()).contains("i33")){
                    valueHolderF=Float.parseFloat(docLatest.get("i33").toString());
                    if(!Float.isNaN(valueHolderF)){
                        kwhToday=valueHolderF-kwhTotalYesterday;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            //date assign
            try{
                if(keyset.contains("i32")){
                    mongoDate=docLatest.get("i32").toString();
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            if(irr_avg>0.0 && invDCCapacity>0.0){
                invPR=(kwhToday/(irr_avg*invDCCapacity))*100;
            }
            dateID=plantID+"-"+invID+"-"+todayD;

            boolean match=false;
            invDailyRs.beforeFirst();
            while(invDailyRs.next()){
                if((invDailyRs.getString("dateid")).contentEquals(dateID)){
                    String ID=invDailyRs.getString("id");
                    sqlupdateandquery.sqlInvDailyUpdate(conn,ID,kwhToday,kwhTodayMinCl,kwhTotal,currDT,ACPow,invStatus,DCPow,invTemp,mongoDate,invPR,todayD,dateID,ACCurr_L1,ACCurr_L2,ACCurr_L3,ACVolt_L1,ACVolt_L2,ACVolt_L3,DCCurr,DCVolt,invAvaRatio,dcEnergy);
                    match=true;
                    break;
                }
            }
            if(!match){
                sqlupdateandquery.sqlInvDailyEntry(conn,kwhToday,kwhTodayMinCl,kwhTotal,currDT,invID,currDT,ACPow,invStatus,plantID,DCPow,invTemp,mongoDate,invPR,todayD,dateID,ACCurr_L1,ACCurr_L2,ACCurr_L3,ACVolt_L1,ACVolt_L2,ACVolt_L3,DCCurr,DCVolt,invAvaRatio,dcEnergy);
            }
            
        }
        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
            stmt4.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
    
    public void smbDaily(List<Document> docListMongoLatest, String siteID, String siteZone, Connection conn, MongoCollection mongocollection, Instant todayStartDT) throws SQLException, ParseException{
        List<Document> docListMongo=new ArrayList<>();
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String currDT=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        
        String smbDailyQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_daily where plant_id=%s and today='%s';", siteID, todayD);  
        String smbDevDetailQ=String.format("SELECT SCB_id, scb_dc_capacity FROM swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_list where plant_id=%s", siteID);  
        String wmsDailyQ=String.format("SELECT wlist.wms_id,wdaily.poa_irradiance,wdaily.today_date FROM `Flexi_solar_wms_list` AS wlist JOIN `Flexi_solar_wms_daily` AS wdaily ON wdaily.plant_id = wlist.plant_id AND wdaily.wms_id=wlist.wms_id WHERE wlist.plant_id=%s AND wlist.WMS_Cumulative=1 AND wdaily.Today=%s;", siteID, todayD);  
        
        ResultSet smbDailyRs=stmt1.executeQuery(smbDailyQ);
        ResultSet smbDetailRs=stmt2.executeQuery(smbDevDetailQ);
        ResultSet wmsDailyRs=stmt3.executeQuery(wmsDailyQ);
        
        //variable define
        ArrayList<String> stringCurrTag = new ArrayList<String>(Arrays.asList("s11","s12","s13","s14","s15","s16","s17","s18","s19","s20","s21","s22","s23","s24","s25","s26","s27","s28","s29","s30","s31","s32","s33","s34"));
        String device;
        String smbID;
        String plantID=null;

        String spdStatus;
        String switchStatus;
        float stringCurr1;
        float stringCurr2;
        float stringCurr3;
        float stringCurr4;
        float stringCurr5;
        float stringCurr6;
        float stringCurr7;
        float stringCurr8;
        float stringCurr9;
        float stringCurr10;
        float stringCurr11;
        float stringCurr12;
        float stringCurr13;
        float stringCurr14;
        float stringCurr15;
        float stringCurr16;
        float stringCurr17;
        float stringCurr18;
        float stringCurr19;
        float stringCurr20;
        float stringCurr21;
        float stringCurr22;
        float stringCurr23;
        float stringCurr24;
        float smbTotCurr;
        float smbVoltage;
        float smbPow;
        float smbDCEnergy;
        float smbPR;
        float valueHolderF=0.0f;
        String valueHolderS=null;
        Set keyset=null;
        float smbDCCapacity=0.0f;
        float irr_avg=0.0f;
        float irrTot;
        int irrC;
        String dateID;
        
        //irrediation
        wmsDailyRs.beforeFirst();
        irrTot=0.0f;
        irrC=0;
        while(wmsDailyRs.next()){
            valueHolderS=wmsDailyRs.getString("poa_irradiance");
            if(valueHolderS!=null){
               irrTot=irrTot+Float.parseFloat(valueHolderS);
               irrC++;
            }
        }

        if(irrC>0){
            irr_avg=irrTot/irrC;
        }

        //latest data
        for(Document docLatest:docListMongoLatest){
            device=docLatest.getString("device");
            smbID=null;
            plantID=null;
            spdStatus="0";
            switchStatus="0";
            stringCurr1=0.0f;
            stringCurr2=0.0f;
            stringCurr3=0.0f;
            stringCurr4=0.0f;
            stringCurr5=0.0f;
            stringCurr6=0.0f;
            stringCurr7=0.0f;
            stringCurr8=0.0f;
            stringCurr9=0.0f;
            stringCurr10=0.0f;
            stringCurr11=0.0f;
            stringCurr12=0.0f;
            stringCurr13=0.0f;
            stringCurr14=0.0f;
            stringCurr15=0.0f;
            stringCurr16=0.0f;
            stringCurr17=0.0f;
            stringCurr18=0.0f;
            stringCurr19=0.0f;
            stringCurr20=0.0f;
            stringCurr21=0.0f;
            stringCurr22=0.0f;
            stringCurr23=0.0f;
            stringCurr24=0.0f;
            smbTotCurr=0.0f;
            smbVoltage=0.0f;
            smbPow=0.0f;
            smbDCEnergy=0.0f;
            smbPR=0.0f;
            smbDCCapacity=0.0f;
            dateID=null;
            
            try{
                String[] dev=device.split("_");
                plantID=dev[0];
                smbID=dev[2];
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            //smbDCCapacity
            smbDetailRs.beforeFirst();
            while(smbDetailRs.next()){
                if(smbDetailRs.getString("SCB_id").contentEquals(smbID)){
                    valueHolderS=smbDetailRs.getString("scb_dc_capacity");
                    if(valueHolderS!=null){
                       smbDCCapacity=Float.parseFloat(valueHolderS);
                    }
                    break;
                }
            }

            docListMongo=getdata.mongoDevGteData1(device, todayStartDT, mongocollection, conn, siteID);
            ////today dc kwh energy
            float dcEnergyCumm=0.0f;
            for(Document doc:docListMongo){
                keyset=doc.keySet();
                try{
                    if(keyset.contains("s39")){
                        try{
                            valueHolderF=Float.parseFloat(doc.get("s39").toString());
                        }
                        catch (Exception ex) {
                             System.out.println("Catch in SMB dvcumm energy s39");
                        }
                        if(!Float.isNaN(valueHolderF)){
                            dcEnergyCumm=dcEnergyCumm+valueHolderF;
                        }
                    }
                    else if(keyset.contains("s35") && keyset.contains("s41")){
                        try{
                            valueHolderF=(Float.parseFloat(doc.get("s35").toString())*Float.parseFloat(doc.get("s41").toString()))/1000;
                        }
                        catch (Exception ex) {
                             System.out.println("Catch in SMB dvcumm energy s35 s41");
                        }
                        if(!Float.isNaN(valueHolderF)){
                            dcEnergyCumm=dcEnergyCumm+valueHolderF;
                        }
                    }
                    else if(keyset.contains("s35")){
                        float totCurr=0.0f;
                        float dcEnergy=0.0f;
                        for(String tag:stringCurrTag){
                            if(keyset.contains(tag)){
                                try{
                                    valueHolderF=Float.parseFloat(doc.get(tag).toString());
                                }
                                catch (Exception ex) {
                                     System.out.println("Catch in SMB dvcumm energy totcurr");
                                }
                                if(!Float.isNaN(valueHolderF)){
                                    totCurr=totCurr+valueHolderF;
                                }
                            }
                        }
                        try{
                            if(keyset.contains("s35")){
                                valueHolderF=Float.parseFloat(doc.get("s35").toString());
                            }
                            else{
                                valueHolderF=0.0f;
                            }
                        }
                        catch (Exception ex) {
                             System.out.println("Catch in SMB dvcumm energy s35");
                        }
                        if(!Float.isNaN(valueHolderF)){
                            dcEnergy=(totCurr*valueHolderF)/1000;
                        }
                        dcEnergyCumm=dcEnergyCumm+dcEnergy;
                    }
                }
                catch (Exception ex) {
                    System.out.println("Catch in SMB dvcumm energy");
                }
            }
            smbDCEnergy=dcEnergyCumm/60;
            
            //other tag data from latest
            keyset=docLatest.keySet();
            try{
                if(keyset.contains("s9")){
                    valueHolderS=docLatest.get("s9").toString();
                    if(!valueHolderS.contentEquals("NaN")){
                        spdStatus=valueHolderS;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s10")){
                    valueHolderS=docLatest.get("s10").toString();
                    if(!valueHolderS.contentEquals("NaN")){
                        switchStatus=valueHolderS;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s11")){
                    valueHolderF=Float.parseFloat(docLatest.get("s11").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr1=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s12")){
                    valueHolderF=Float.parseFloat(docLatest.get("s12").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr2=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s13")){
                    valueHolderF=Float.parseFloat(docLatest.get("s13").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr3=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s14")){
                    valueHolderF=Float.parseFloat(docLatest.get("s14").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr4=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s15")){
                    valueHolderF=Float.parseFloat(docLatest.get("s15").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr5=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s16")){
                    valueHolderF=Float.parseFloat(docLatest.get("s16").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr6=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s17")){
                    valueHolderF=Float.parseFloat(docLatest.get("s17").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr7=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s18")){
                    valueHolderF=Float.parseFloat(docLatest.get("s18").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr8=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s19")){
                    valueHolderF=Float.parseFloat(docLatest.get("s19").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr9=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s20")){
                    valueHolderF=Float.parseFloat(docLatest.get("s20").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr10=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s21")){
                    valueHolderF=Float.parseFloat(docLatest.get("s21").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr11=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s22")){
                    valueHolderF=Float.parseFloat(docLatest.get("s22").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr12=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s23")){
                    valueHolderF=Float.parseFloat(docLatest.get("s23").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr13=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s24")){
                    valueHolderF=Float.parseFloat(docLatest.get("s24").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr14=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s25")){
                    valueHolderF=Float.parseFloat(docLatest.get("s25").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr15=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s26")){
                    valueHolderF=Float.parseFloat(docLatest.get("s26").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr16=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s27")){
                    valueHolderF=Float.parseFloat(docLatest.get("s27").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr17=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s28")){
                    valueHolderF=Float.parseFloat(docLatest.get("s28").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr18=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s29")){
                    valueHolderF=Float.parseFloat(docLatest.get("s29").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr19=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }

            try{
                if(keyset.contains("s30")){
                valueHolderF=Float.parseFloat(docLatest.get("s30").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr20=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s31")){
                    valueHolderF=Float.parseFloat(docLatest.get("s31").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr21=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s32")){
                    valueHolderF=Float.parseFloat(docLatest.get("s32").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr22=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s33")){
                    valueHolderF=Float.parseFloat(docLatest.get("s33").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr23=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s34")){
                    valueHolderF=Float.parseFloat(docLatest.get("s34").toString());
                    if(!Float.isNaN(valueHolderF)){
                        stringCurr24=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s35")){
                    valueHolderF=Float.parseFloat(docLatest.get("s35").toString());
                    if(!Float.isNaN(valueHolderF)){
                        smbVoltage=valueHolderF;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s41")){
                    valueHolderF=Float.parseFloat(docLatest.get("s41").toString());
                    if(!Float.isNaN(valueHolderF)){
                        smbTotCurr=valueHolderF;
                    }
                }
                else{
                    smbTotCurr=stringCurr1+stringCurr2+stringCurr3+stringCurr4+stringCurr5+stringCurr6+stringCurr7+stringCurr8+stringCurr9+stringCurr10+stringCurr11+stringCurr12+stringCurr13
                            +stringCurr14+stringCurr15+stringCurr16+stringCurr17+stringCurr18+stringCurr19+stringCurr20+stringCurr21+stringCurr22+stringCurr23+stringCurr24;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(keyset.contains("s39")){
                    valueHolderF=Float.parseFloat(docLatest.get("s39").toString());
                    if(!Float.isNaN(valueHolderF)){
                        smbPow=valueHolderF;
                    }
                }
                else{
                    smbPow=(smbTotCurr*smbVoltage)/1000;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            if(irr_avg>0.0 && smbDCCapacity>0.0){
                smbPR=(smbDCEnergy/(irr_avg*smbDCCapacity))*100;
            }
            dateID=plantID+"-"+smbID+"-"+todayD;
            
            boolean match=false;
            smbDailyRs.beforeFirst();
            while(smbDailyRs.next()){
                if((smbDailyRs.getString("dateid")).contentEquals(dateID)){
                    String ID=smbDailyRs.getString("id");
                    sqlupdateandquery.sqlSmbDailyUpdate(conn,ID,currDT,spdStatus,switchStatus,stringCurr1,stringCurr2,stringCurr3,stringCurr4,stringCurr5,stringCurr6,stringCurr7,stringCurr8,stringCurr9,stringCurr10,stringCurr11,stringCurr12,stringCurr13,stringCurr14,stringCurr15,stringCurr16,stringCurr17,stringCurr18,stringCurr19,stringCurr20,stringCurr21,stringCurr22,stringCurr23,stringCurr24,smbVoltage,smbTotCurr,smbPow,smbPR,smbDCEnergy,siteID);
                    match=true;
                    break;
                }
            }
            if(!match){
                sqlupdateandquery.sqlSmbDailyEntry(conn,dateID,plantID,smbID,currDT,todayD,spdStatus,switchStatus,stringCurr1,stringCurr2,stringCurr3,stringCurr4,stringCurr5,stringCurr6,stringCurr7,stringCurr8,stringCurr9,stringCurr10,stringCurr11,stringCurr12,stringCurr13,stringCurr14,stringCurr15,stringCurr16,stringCurr17,stringCurr18,stringCurr19,stringCurr20,stringCurr21,stringCurr22,stringCurr23,stringCurr24,smbVoltage,smbTotCurr,smbPow,smbPR,smbDCEnergy,siteID);
            }
            
            
        }
        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
    
}