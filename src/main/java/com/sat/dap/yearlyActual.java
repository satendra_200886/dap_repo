package com.sat.dap;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.distribution.NormalDistribution;

public class yearlyActual {
    sqlUpdateAndEntry sqlupdateandquery= new sqlUpdateAndEntry();
    
    public void yearlyPfactor(String siteID, String siteZone,Connection conn) throws SQLException{
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String currDT=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String dateIYY=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy"));
        String dateIDY=zoneddatetime.format(DateTimeFormatter.ofPattern("ddyyyy"));
        int year=zoneddatetime.getYear();
        int month=zoneddatetime.getMonthValue();
        
        List<Double> probList=new ArrayList<>();
        List<String> pValue=new ArrayList<>();
        probList.add(0.95);
        probList.add(0.9);
        probList.add(0.85);
        probList.add(0.8);
        probList.add(0.75);
        probList.add(0.7);
        probList.add(0.65);
        probList.add(0.6);
        probList.add(0.55);
        probList.add(0.5);
        probList.add(0.45);
        probList.add(0.4);
        probList.add(0.35);
        probList.add(0.3);
        probList.add(0.25);
        probList.add(0.2);
        probList.add(0.15);
        probList.add(0.1);
        probList.add(0.05);
        probList.add(0.01);
        pValue.add("5");
        pValue.add("10");
        pValue.add("15");
        pValue.add("20");
        pValue.add("25");
        pValue.add("30");
        pValue.add("35");
        pValue.add("40");
        pValue.add("45");
        pValue.add("50");
        pValue.add("55");
        pValue.add("60");
        pValue.add("65");
        pValue.add("70");
        pValue.add("75");
        pValue.add("80");
        pValue.add("85");
        pValue.add("90");
        pValue.add("95");
        pValue.add("99");
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        
        String estimYearQ=String.format("SELECT SUM(kWh_month_p50) as kWh,DATE_FORMAT(date_time,'%s') AS showdate,plant_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_monthly_estimation_data where year(date_time)=%s and month(date_time)<=%s and plant_id=%s GROUP BY YEAR(date_time);", "%Y", year, month, siteID, dateIDY);
        String actualYearkWhQ=String.format("SELECT SUM(kWh_today_export) as kWh,DATE_FORMAT(today_date,'%s') AS showdate FROM swlflexi_CMSswlawsReMACS.Flexi_solar_daily_actual_data where year(today_date)=%s and plant_id=%s GROUP BY YEAR(today_date);", "%Y", year, siteID);  
        String actualYearQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_yearly_actual_data where plant_id=%s and today=%s;", siteID, dateIYY);  

        ResultSet estimYearRs=stmt1.executeQuery(estimYearQ);
        ResultSet actualYearkWhRs=stmt2.executeQuery(actualYearkWhQ);
        ResultSet actualYearRs=stmt3.executeQuery(actualYearQ);

        float estimatedP50=0.0f;
        double actualYearlykWh=0.0;
        String pStatus="";
        String valueHolderS="";
        String dateID="";

        try{
            while(estimYearRs.next()){
                if(estimYearRs.getString("plant_id").contentEquals(siteID)){
                    valueHolderS=estimYearRs.getString("kWh");
                    if(valueHolderS!=null){
                        estimatedP50= Float.parseFloat(valueHolderS);
                    }
                    break;
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        try{
            while(actualYearkWhRs.next()){
                valueHolderS=actualYearkWhRs.getString("kWh");
                if(valueHolderS!=null){
                    actualYearlykWh= Double.parseDouble(valueHolderS);
                }
                break;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        NormalDistribution distribution=null;
        try{
            distribution = new NormalDistribution(estimatedP50, estimatedP50*0.066);
            
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        for(int i=0; i<probList.size(); i++){
            if(actualYearlykWh>(distribution.inverseCumulativeProbability(probList.get(i)))){
                pStatus=pValue.get(i);
                break;
            }
        }
        if(pStatus.isEmpty()){
            pStatus="99"; 
        }

        dateID=siteID+"-"+dateIYY;
        boolean match=false;
        actualYearkWhRs.beforeFirst();
        while(actualYearRs.next()){
            if((actualYearRs.getString("dateid")).contentEquals(dateID)){
                String ID=actualYearRs.getString("id");
                sqlupdateandquery.actualYearlyUpdate(conn, currDT, actualYearlykWh, pStatus, ID);
                match=true;
                break;
            }
        }
        if(!match){
            sqlupdateandquery.actualYearlyEntry(conn,siteID,currDT,dateIYY,dateID,actualYearlykWh, pStatus);
        }

        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }  
        
    }
    
}
