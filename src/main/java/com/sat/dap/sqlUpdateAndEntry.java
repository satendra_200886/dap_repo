
package com.sat.dap;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;

public class sqlUpdateAndEntry {
    
    public void sqlInvDailyUpdate(Connection conn,String ID,float kwhToday,float kwhTodayMinCl,float kwhTotal,String updateTime,float ACPow,int invStatus,float DCPow,float invTemp,String mongoDate,float invPR,String todayD,String dateID,float ACCurr_L1,float ACCurr_L2,float ACCurr_L3,float ACVolt_L1,float ACVolt_L2,float ACVolt_L3,float DCCurr,float DCVolt,float invAvaRatio,float dcEnergy) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=null;
        if(0<=invPR && invPR<=200){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_inverters_daily SET kwh_today=%s,kwh_today_mincl=%s,kwh_till_date=%s,update_time='%s',AC_Power=%s,inverter_status=%s,DC_Power=%s,inv_Temperature=%s,last_data_time='%s',Inv_PR=%s,Today='%s',dateid='%s',AC_Current_line1=%s,AC_Current_line2=%s,AC_Current_line3=%s,AC_Voltage_line1=%s,AC_Voltage_line2=%s,AC_Voltage_line3=%s,DC_Current=%s,DC_VOLTAGE=%s,inv_ava_ratio=%s,today_dc_energy=%s WHERE id=%s;", kwhToday,kwhTodayMinCl,kwhTotal,updateTime,ACPow,invStatus,DCPow,invTemp,mongoDate,invPR,todayD,dateID,ACCurr_L1,ACCurr_L2,ACCurr_L3,ACVolt_L1,ACVolt_L2,ACVolt_L3,DCCurr,DCVolt,invAvaRatio,dcEnergy,ID);
        }
        else{
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_inverters_daily SET kwh_today=%s,kwh_today_mincl=%s,kwh_till_date=%s,update_time='%s',AC_Power=%s,inverter_status=%s,DC_Power=%s,inv_Temperature=%s,last_data_time='%s',Today='%s',dateid='%s',AC_Current_line1=%s,AC_Current_line2=%s,AC_Current_line3=%s,AC_Voltage_line1=%s,AC_Voltage_line2=%s,AC_Voltage_line3=%s,DC_Current=%s,DC_VOLTAGE=%s,inv_ava_ratio=%s,today_dc_energy=%s  WHERE id=%s;", kwhToday,kwhTodayMinCl,kwhTotal,updateTime,ACPow,invStatus,DCPow,invTemp,mongoDate,todayD,dateID,ACCurr_L1,ACCurr_L2,ACCurr_L3,ACVolt_L1,ACVolt_L2,ACVolt_L3,DCCurr,DCVolt,invAvaRatio,dcEnergy,ID);
        }

        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    
    public void sqlInvDailyEntry(Connection conn,float kwhToday,float kwhTodayMinCl,float kwhTotal,String updateTime,String invId,String todayDate,float ACPow,int invStatus,String plantId,float DCPow,float invTemp,String mongoDate,float invPR,String todayD,String dateID,float ACCurr_L1,float ACCurr_L2,float ACCurr_L3,float ACVolt_L1,float ACVolt_L2,float ACVolt_L3,float DCCurr,float DCVolt,float invAvaRatio,float dcEnergy) throws SQLException{
        Statement stmt= conn.createStatement();
        String entryQuery=null;
        if(0<=invPR && invPR<=200){
            entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_inverters_daily (kwh_today,kwh_today_mincl,kwh_till_date,update_time,Inverter_id,today_date,AC_Power,inverter_status,plant_id,DC_Power,inv_Temperature,last_data_time,Inv_PR,Today,dateid,AC_Current_line1,AC_Current_line2,AC_Current_line3,AC_Voltage_line1,AC_Voltage_line2,AC_Voltage_line3,DC_Current,DC_VOLTAGE,inv_ava_ratio,today_dc_energy) VALUES (%s,%s,%s,'%s','%s','%s',%s,%s,'%s',%s,%s,'%s',%s,'%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", kwhToday,kwhTodayMinCl,kwhTotal,updateTime,invId,todayDate,ACPow,invStatus,plantId,DCPow,invTemp,mongoDate,invPR,todayD,dateID,ACCurr_L1,ACCurr_L2,ACCurr_L3,ACVolt_L1,ACVolt_L2,ACVolt_L3,DCCurr,DCVolt,invAvaRatio,dcEnergy);
        }
        else{
            entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_inverters_daily (kwh_today,kwh_today_mincl,kwh_till_date,update_time,Inverter_id,today_date,AC_Power,inverter_status,plant_id,DC_Power,inv_Temperature,last_data_time,Today,dateid,AC_Current_line1,AC_Current_line2,AC_Current_line3,AC_Voltage_line1,AC_Voltage_line2,AC_Voltage_line3,DC_Current,DC_VOLTAGE,inv_ava_ratio,today_dc_energy) VALUES (%s,%s,%s,'%s','%s','%s',%s,%s,'%s',%s,%s,'%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", kwhToday,kwhTodayMinCl,kwhTotal,updateTime,invId,todayDate,ACPow,invStatus,plantId,DCPow,invTemp,mongoDate,todayD,dateID,ACCurr_L1,ACCurr_L2,ACCurr_L3,ACVolt_L1,ACVolt_L2,ACVolt_L3,DCCurr,DCVolt,invAvaRatio,dcEnergy);
        }

        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    public void sqlSmbDailyUpdate(Connection conn,String ID,String currDT,String spdStatus,String switchStatus,float stringCurr1,float stringCurr2,float stringCurr3,float stringCurr4,float stringCurr5,float stringCurr6,float stringCurr7,float stringCurr8,float stringCurr9,float stringCurr10,float stringCurr11,float stringCurr12,float stringCurr13,float stringCurr14,float stringCurr15,float stringCurr16,float stringCurr17,float stringCurr18,float stringCurr19,float stringCurr20,float stringCurr21,float stringCurr22,float stringCurr23,float stringCurr24,float smb_voltage,float smb_tot_curr,float smb_pow,float smbPR,float smbDCEnergy,String siteID) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=null;
        if(0<=smbPR && smbPR<=100){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_daily SET update_date='%s',spd_status=%s,switch_status=%s,string_current_1=%s,string_current_2=%s,string_current_3=%s,string_current_4=%s,string_current_5=%s,string_current_6=%s,string_current_7=%s,string_current_8=%s,string_current_9=%s,string_current_10=%s,string_current_11=%s,string_current_12=%s,string_current_13=%s,string_current_14=%s,string_current_15=%s,string_current_16=%s,string_current_17=%s,string_current_18=%s,string_current_19=%s,string_current_20=%s,string_current_21=%s,string_current_22=%s,string_current_23=%s,string_current_24=%s,smb_voltage=%s,smb_total_current=%s,smb_power=%s,scb_PR=%s,scb_kwh=%s WHERE id=%s;", currDT,spdStatus,switchStatus,stringCurr1,stringCurr2,stringCurr3,stringCurr4,stringCurr5,stringCurr6,stringCurr7,stringCurr8,stringCurr9,stringCurr10,stringCurr11,stringCurr12,stringCurr13,stringCurr14,stringCurr15,stringCurr16,stringCurr17,stringCurr18,stringCurr19,stringCurr20,stringCurr21,stringCurr22,stringCurr23,stringCurr24,smb_voltage,smb_tot_curr,smb_pow,smbPR,smbDCEnergy,ID);
        }
        else{
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_daily SET update_date='%s',spd_status=%s,switch_status=%s,string_current_1=%s,string_current_2=%s,string_current_3=%s,string_current_4=%s,string_current_5=%s,string_current_6=%s,string_current_7=%s,string_current_8=%s,string_current_9=%s,string_current_10=%s,string_current_11=%s,string_current_12=%s,string_current_13=%s,string_current_14=%s,string_current_15=%s,string_current_16=%s,string_current_17=%s,string_current_18=%s,string_current_19=%s,string_current_20=%s,string_current_21=%s,string_current_22=%s,string_current_23=%s,string_current_24=%s,smb_voltage=%s,smb_total_current=%s,smb_power=%s,scb_kwh=%s WHERE id=%s;", currDT,spdStatus,switchStatus,stringCurr1,stringCurr2,stringCurr3,stringCurr4,stringCurr5,stringCurr6,stringCurr7,stringCurr8,stringCurr9,stringCurr10,stringCurr11,stringCurr12,stringCurr13,stringCurr14,stringCurr15,stringCurr16,stringCurr17,stringCurr18,stringCurr19,stringCurr20,stringCurr21,stringCurr22,stringCurr23,stringCurr24,smb_voltage,smb_tot_curr,smb_pow,smbDCEnergy,ID);
        }
        
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
            System.out.println(siteID);
            System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    
    public void sqlSmbDailyEntry(Connection conn,String dateID,String PlantID,String smbID,String currDT,String todayD,String spdStatus,String switchStatus,float stringCurr1,float stringCurr2,float stringCurr3,float stringCurr4,float stringCurr5,float stringCurr6,float stringCurr7,float stringCurr8,float stringCurr9,float stringCurr10,float stringCurr11,float stringCurr12,float stringCurr13,float stringCurr14,float stringCurr15,float stringCurr16,float stringCurr17,float stringCurr18,float stringCurr19,float stringCurr20,float stringCurr21,float stringCurr22,float stringCurr23,float stringCurr24,float smb_voltage,float smb_tot_curr,float smb_pow,float smbPR,float smbDCEnergy,String siteID) throws SQLException{
        Statement stmt= conn.createStatement();
        String entryQuery=null;
        if(0<=smbPR && smbPR<=100){
            entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_daily (dateid,plant_id,smb_id,today_date,update_date,today,spd_status,switch_status,string_current_1,string_current_2,string_current_3,string_current_4,string_current_5,string_current_6,string_current_7,string_current_8,string_current_9,string_current_10,string_current_11,string_current_12,string_current_13,string_current_14,string_current_15,string_current_16,string_current_17,string_current_18,string_current_19,string_current_20,string_current_21,string_current_22,string_current_23,string_current_24,smb_voltage,smb_total_current,smb_power,scb_PR,scb_kwh) VALUES ('%s',%s,%s,'%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", dateID,PlantID,smbID,currDT,currDT,todayD,spdStatus,switchStatus,stringCurr1,stringCurr2,stringCurr3,stringCurr4,stringCurr5,stringCurr6,stringCurr7,stringCurr8,stringCurr9,stringCurr10,stringCurr11,stringCurr12,stringCurr13,stringCurr14,stringCurr15,stringCurr16,stringCurr17,stringCurr18,stringCurr19,stringCurr20,stringCurr21,stringCurr22,stringCurr23,stringCurr24,smb_voltage,smb_tot_curr,smb_pow,smbPR,smbDCEnergy);
        }
        else{
            entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_SCB_daily (dateid,plant_id,smb_id,today_date,update_date,today,spd_status,switch_status,string_current_1,string_current_2,string_current_3,string_current_4,string_current_5,string_current_6,string_current_7,string_current_8,string_current_9,string_current_10,string_current_11,string_current_12,string_current_13,string_current_14,string_current_15,string_current_16,string_current_17,string_current_18,string_current_19,string_current_20,string_current_21,string_current_22,string_current_23,string_current_24,smb_voltage,smb_total_current,smb_power,scb_kwh) VALUES ('%s',%s,%s,'%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", dateID,PlantID,smbID,currDT,currDT,todayD,spdStatus,switchStatus,stringCurr1,stringCurr2,stringCurr3,stringCurr4,stringCurr5,stringCurr6,stringCurr7,stringCurr8,stringCurr9,stringCurr10,stringCurr11,stringCurr12,stringCurr13,stringCurr14,stringCurr15,stringCurr16,stringCurr17,stringCurr18,stringCurr19,stringCurr20,stringCurr21,stringCurr22,stringCurr23,stringCurr24,smb_voltage,smb_tot_curr,smb_pow,smbDCEnergy);
        }
        
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
            System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    
    public void sqlWmsDailyUpdate(ResultSet wmsDailyRs,Connection conn,String ID,String currDT,String mongoDT,float poaIrradiance,float lossIrrExcBD,float irrTilt1,float irrTilt1Avg,float irrTilt2,float irrTilt2Avg,float irrTiltAvgInst,float irrTiltCAvgExcBD,float poaIrrEve,float ghiIrradiance,float irrGlobal,float irrGlobalAvg,float ghiIrrEve,float ambTemp,float ambTempAvg,float modTemp1,float modTemp1Avg,float modTemp2,float modTemp2Avg,float modTempAvgInst,float modTempExcBD,float humidity,float humidityAvg,float windSpeed,float windSpeedAvg,float windDir,float windDirAvg,float roomTemp,float roomTempAvg,float ghiPoaGain,String sunriseTimeAPI,String sunsetTimeAPI,String sunriseTimeStored,String sunsetTimeStored,String sunriseTimeUpdate,String sunsetTimeUpdate,String plantWakeupTStored,String plantSleepTStored,float acPower, float kwhExpected, float expectedPowIns, float lossEnergy) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=null;
        if(sunriseTimeStored==null){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily SET update_date='%s',last_data_time='%s',poa_irradiance=%s,loss_irradiance_ext_bd=%s,irradiation_tilt1=%s,irradiation_tilt1_avg=%s,irradiation_tilt2=%s,irradiation_tilt2_avg=%s,irradiation_tilt12_avg_inst=%s,irr_tiltavg_exc_bd=%s,poa_irr_eve=%s,global_irradiance=%s,gloabal_irradiation=%s,global_irradiation_avg=%s,ghi_irr_eve=%s,ambient_tmp=%s,ambient_tmp_avg=%s,module1_tmp=%s,module1_tmp_avg=%s,module2_tmp=%s,module2_tmp_avg=%s,mod_temp_avg=%s,mod_tmpavg_exc_bd=%s,humidity=%s,humidity_avg=%s,wind_speed=%s,wind_speed_avg=%s,wind_direction=%s,wind_direction_avg=%s,room_temperature=%s,room_temperature_avg=%s,GHI_POA_Gain=%s,expected_energy=%s,expected_power=%s,loss_energy=%s,plant_sunrise_time='%s',plant_sunset_time='%s',sunrise_Time_updated=%s WHERE id=%s;", currDT,mongoDT,poaIrradiance,lossIrrExcBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTemp1Avg,modTemp2,modTemp2Avg,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,kwhExpected,expectedPowIns,lossEnergy,sunriseTimeAPI,sunsetTimeAPI,"0",ID);
        }
        else if(irrTiltAvgInst>0.0 && !sunriseTimeUpdate.contentEquals("1")){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily SET update_date='%s',last_data_time='%s',poa_irradiance=%s,loss_irradiance_ext_bd=%s,irradiation_tilt1=%s,irradiation_tilt1_avg=%s,irradiation_tilt2=%s,irradiation_tilt2_avg=%s,irradiation_tilt12_avg_inst=%s,irr_tiltavg_exc_bd=%s,poa_irr_eve=%s,global_irradiance=%s,gloabal_irradiation=%s,global_irradiation_avg=%s,ghi_irr_eve=%s,ambient_tmp=%s,ambient_tmp_avg=%s,module1_tmp=%s,module1_tmp_avg=%s,module2_tmp=%s,module2_tmp_avg=%s,mod_temp_avg=%s,mod_tmpavg_exc_bd=%s,humidity=%s,humidity_avg=%s,wind_speed=%s,wind_speed_avg=%s,wind_direction=%s,wind_direction_avg=%s,room_temperature=%s,room_temperature_avg=%s,GHI_POA_Gain=%s,expected_energy=%s,expected_power=%s,loss_energy=%s,plant_sunrise_time='%s',sunrise_Time_updated=%s WHERE id=%s;", currDT,mongoDT,poaIrradiance,lossIrrExcBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTemp1Avg,modTemp2,modTemp2Avg,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,kwhExpected,expectedPowIns,lossEnergy,currDT,"1",ID);
        }
        else if(acPower>0.1 && plantWakeupTStored.contentEquals("0000-00-00 00:00:00")){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily SET update_date='%s',last_data_time='%s',poa_irradiance=%s,loss_irradiance_ext_bd=%s,irradiation_tilt1=%s,irradiation_tilt1_avg=%s,irradiation_tilt2=%s,irradiation_tilt2_avg=%s,irradiation_tilt12_avg_inst=%s,irr_tiltavg_exc_bd=%s,poa_irr_eve=%s,global_irradiance=%s,gloabal_irradiation=%s,global_irradiation_avg=%s,ghi_irr_eve=%s,ambient_tmp=%s,ambient_tmp_avg=%s,module1_tmp=%s,module1_tmp_avg=%s,module2_tmp=%s,module2_tmp_avg=%s,mod_temp_avg=%s,mod_tmpavg_exc_bd=%s,humidity=%s,humidity_avg=%s,wind_speed=%s,wind_speed_avg=%s,wind_direction=%s,wind_direction_avg=%s,room_temperature=%s,room_temperature_avg=%s,GHI_POA_Gain=%s,expected_energy=%s,expected_power=%s,loss_energy=%s,plant_wakeup_time='%s',sunset_Time_updated=%s WHERE id=%s;", currDT,mongoDT,poaIrradiance,lossIrrExcBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTemp1Avg,modTemp2,modTemp2Avg,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,kwhExpected,expectedPowIns,lossEnergy,currDT,"1",ID);
        }
        else if(acPower<0.1 && irrTiltAvgInst<2.5 && plantSleepTStored.contentEquals("0000-00-00 00:00:00") && !plantWakeupTStored.contentEquals("0000-00-00 00:00:00")){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily SET update_date='%s',last_data_time='%s',poa_irradiance=%s,loss_irradiance_ext_bd=%s,irradiation_tilt1=%s,irradiation_tilt1_avg=%s,irradiation_tilt2=%s,irradiation_tilt2_avg=%s,irradiation_tilt12_avg_inst=%s,irr_tiltavg_exc_bd=%s,poa_irr_eve=%s,global_irradiance=%s,gloabal_irradiation=%s,global_irradiation_avg=%s,ghi_irr_eve=%s,ambient_tmp=%s,ambient_tmp_avg=%s,module1_tmp=%s,module1_tmp_avg=%s,module2_tmp=%s,module2_tmp_avg=%s,mod_temp_avg=%s,mod_tmpavg_exc_bd=%s,humidity=%s,humidity_avg=%s,wind_speed=%s,wind_speed_avg=%s,wind_direction=%s,wind_direction_avg=%s,room_temperature=%s,room_temperature_avg=%s,GHI_POA_Gain=%s,expected_energy=%s,expected_power=%s,loss_energy=%s,plant_sleep_time='%s' WHERE id=%s;", currDT,mongoDT,poaIrradiance,lossIrrExcBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTemp1Avg,modTemp2,modTemp2Avg,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,kwhExpected,expectedPowIns,lossEnergy,currDT,ID);
        }
        else if(sunsetTimeUpdate.contentEquals("1") && irrTiltAvgInst<0.1){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily SET update_date='%s',last_data_time='%s',poa_irradiance=%s,loss_irradiance_ext_bd=%s,irradiation_tilt1=%s,irradiation_tilt1_avg=%s,irradiation_tilt2=%s,irradiation_tilt2_avg=%s,irradiation_tilt12_avg_inst=%s,irr_tiltavg_exc_bd=%s,poa_irr_eve=%s,global_irradiance=%s,gloabal_irradiation=%s,global_irradiation_avg=%s,ghi_irr_eve=%s,ambient_tmp=%s,ambient_tmp_avg=%s,module1_tmp=%s,module1_tmp_avg=%s,module2_tmp=%s,module2_tmp_avg=%s,mod_temp_avg=%s,mod_tmpavg_exc_bd=%s,humidity=%s,humidity_avg=%s,wind_speed=%s,wind_speed_avg=%s,wind_direction=%s,wind_direction_avg=%s,room_temperature=%s,room_temperature_avg=%s,GHI_POA_Gain=%s,expected_energy=%s,expected_power=%s,loss_energy=%s,plant_sunset_time='%s',sunset_Time_updated=%s WHERE id=%s;", currDT,mongoDT,poaIrradiance,lossIrrExcBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTemp1Avg,modTemp2,modTemp2Avg,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,kwhExpected,expectedPowIns,lossEnergy,currDT,"0",ID);
        }
        else if(acPower>0.1 && !plantWakeupTStored.contentEquals("0000-00-00 00:00:00") && !plantSleepTStored.contentEquals("0000-00-00 00:00:00")){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily SET update_date='%s',last_data_time='%s',poa_irradiance=%s,loss_irradiance_ext_bd=%s,irradiation_tilt1=%s,irradiation_tilt1_avg=%s,irradiation_tilt2=%s,irradiation_tilt2_avg=%s,irradiation_tilt12_avg_inst=%s,irr_tiltavg_exc_bd=%s,poa_irr_eve=%s,global_irradiance=%s,gloabal_irradiation=%s,global_irradiation_avg=%s,ghi_irr_eve=%s,ambient_tmp=%s,ambient_tmp_avg=%s,module1_tmp=%s,module1_tmp_avg=%s,module2_tmp=%s,module2_tmp_avg=%s,mod_temp_avg=%s,mod_tmpavg_exc_bd=%s,humidity=%s,humidity_avg=%s,wind_speed=%s,wind_speed_avg=%s,wind_direction=%s,wind_direction_avg=%s,room_temperature=%s,room_temperature_avg=%s,GHI_POA_Gain=%s,expected_energy=%s,expected_power=%s,loss_energy=%s,plant_sleep_time='%s',sunset_Time_updated=%s WHERE id=%s;", currDT,mongoDT,poaIrradiance,lossIrrExcBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTemp1Avg,modTemp2,modTemp2Avg,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,kwhExpected,expectedPowIns,lossEnergy,"0000-00-00 00:00:00","1",ID);
        }
        else{
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily SET update_date='%s',last_data_time='%s',poa_irradiance=%s,loss_irradiance_ext_bd=%s,irradiation_tilt1=%s,irradiation_tilt1_avg=%s,irradiation_tilt2=%s,irradiation_tilt2_avg=%s,irradiation_tilt12_avg_inst=%s,irr_tiltavg_exc_bd=%s,poa_irr_eve=%s,global_irradiance=%s,gloabal_irradiation=%s,global_irradiation_avg=%s,ghi_irr_eve=%s,ambient_tmp=%s,ambient_tmp_avg=%s,module1_tmp=%s,module1_tmp_avg=%s,module2_tmp=%s,module2_tmp_avg=%s,mod_temp_avg=%s,mod_tmpavg_exc_bd=%s,humidity=%s,humidity_avg=%s,wind_speed=%s,wind_speed_avg=%s,wind_direction=%s,wind_direction_avg=%s,room_temperature=%s,room_temperature_avg=%s,GHI_POA_Gain=%s,expected_energy=%s,expected_power=%s,loss_energy=%s WHERE id=%s;", currDT,mongoDT,poaIrradiance,lossIrrExcBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTemp1Avg,modTemp2,modTemp2Avg,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,kwhExpected,expectedPowIns,lossEnergy,ID);
        }

        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    
    public void sqlWmsDailyEntry(ResultSet wmsDailyRs,Connection conn,String plantID,String wmsID,String currDT,String mongoDT,String todayD,String dateID,float poaIrradiance,float lossIrrExcBD,float irrTilt1,float irrTilt1Avg,float irrTilt2,float irrTilt2Avg,float irrTiltAvgInst,float irrTiltCAvgExcBD,float poaIrrEve,float ghiIrradiance,float irrGlobal,float irrGlobalAvg,float ghiIrrEve,float ambTemp,float ambTempAvg,float modTemp1,float modTemp1Avg,float modTemp2,float modTemp2Avg,float modTempAvgInst,float modTempExcBD,float humidity,float humidityAvg,float windSpeed,float windSpeedAvg,float windDir,float windDirAvg,float roomTemp,float roomTempAvg,float ghiPoaGain,String sunriseTimeAPI,String sunsetTimeAPI,String sunriseTimeStored,String sunsetTimeStored,String sunriseTimeUpdate,String sunsetTimeUpdate,String plantWakeupTStored,String plantSleepTStored,float acPower, float kwhExpected, float expectedPowIns,  float lossEnergy) throws SQLException{
        Statement stmt= conn.createStatement();
        String entryQuery=null;

        entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_wms_daily (plant_id,wms_id,update_date,today_date,last_data_time,today,dateid,poa_irradiance,loss_irradiance_ext_bd,irradiation_tilt1,irradiation_tilt1_avg,irradiation_tilt2,irradiation_tilt2_avg,irradiation_tilt12_avg_inst,irr_tiltavg_exc_bd,poa_irr_eve,global_irradiance,gloabal_irradiation,global_irradiation_avg,ghi_irr_eve,ambient_tmp,ambient_tmp_avg,module1_tmp,module1_tmp_avg,module2_tmp,module2_tmp_avg,mod_temp_avg,mod_tmpavg_exc_bd,humidity,humidity_avg,wind_speed,wind_speed_avg,wind_direction,wind_direction_avg,room_temperature,room_temperature_avg,GHI_POA_Gain,expected_energy,expected_power,loss_energy) VALUES (%s,%s,'%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", plantID,wmsID,currDT,currDT,mongoDT,todayD,dateID,poaIrradiance,lossIrrExcBD,irrTilt1,irrTilt1Avg,irrTilt2,irrTilt2Avg,irrTiltAvgInst,irrTiltCAvgExcBD,poaIrrEve,ghiIrradiance,irrGlobal,irrGlobalAvg,ghiIrrEve,ambTemp,ambTempAvg,modTemp1,modTemp1Avg,modTemp2,modTemp2Avg,modTempAvgInst,modTempExcBD,humidity,humidityAvg,windSpeed,windSpeedAvg,windDir,windDirAvg,roomTemp,roomTempAvg,ghiPoaGain,kwhExpected,expectedPowIns,lossEnergy);
        
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    
    public void sqlMfmDailyUpdate(Connection conn,String ID,String currDT,String mongoDate,float kwhTodayExp,float kwhTodayImp,float kwhTotalExp,float kwhTotalImp,float ACPower,float MaxACPower,float ReacPower,float mfmPR) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=null;
        if(0<=mfmPR && mfmPR<=100){
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_daily SET update_date='%s',last_data_time='%s',Energy_Export_today=%s,Energy_import_today=%s,Energy_Export_total=%s,Energy_import_total=%s,AC_Power=%s,Max_AC_Power=%s,Reactive_Power=%s,MFM_PR=%s WHERE id=%s;", currDT,mongoDate,kwhTodayExp,kwhTodayImp,kwhTotalExp,kwhTotalImp,ACPower,MaxACPower,ReacPower,mfmPR,ID);
        }
        else{
            updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_daily SET update_date='%s',last_data_time='%s',Energy_Export_today=%s,Energy_import_today=%s,Energy_Export_total=%s,Energy_import_total=%s,AC_Power=%s,Max_AC_Power=%s,Reactive_Power=%s WHERE id=%s;", currDT,mongoDate,kwhTodayExp,kwhTodayImp,kwhTotalExp,kwhTotalImp,ACPower,MaxACPower,ReacPower,ID);
        }
        
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    
    public void sqlMfmDailyEntry(Connection conn,String plantID,String mfmID,String currDT,String todayD,String dateID,String mongoDate,float kwhTodayExp,float kwhTodayImp,float kwhTotalExp,float kwhTotalImp,float ACPower,float MaxACPower,float ReacPower,float mfmPR) throws SQLException{
        Statement stmt= conn.createStatement();
        String entryQuery=null;
        if(0<=mfmPR && mfmPR<=100){
            entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_daily (plant_id,M_id,update_date,today_date,Today,dateid,last_data_time,Energy_Export_today,Energy_import_today,Energy_Export_total,Energy_import_total,AC_Power,Max_AC_Power,Reactive_Power,MFM_PR) VALUES ('%s','%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s)", plantID,mfmID,currDT,currDT,todayD,dateID,mongoDate,kwhTodayExp,kwhTodayImp,kwhTotalExp,kwhTotalImp,ACPower,MaxACPower,ReacPower,mfmPR);
        }
        else{
            entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_daily (plant_id,M_id,update_date,today_date,Today,dateid,last_data_time,Energy_Export_today,Energy_import_today,Energy_Export_total,Energy_import_total,AC_Power,Max_AC_Power,Reactive_Power) VALUES ('%s','%s','%s','%s','%s','%s','%s',%s,%s,%s,%s,%s,%s,%s)", plantID,mfmID,currDT,currDT,todayD,dateID,mongoDate,kwhTodayExp,kwhTodayImp,kwhTotalExp,kwhTotalImp,ACPower,MaxACPower,ReacPower);
        }
        
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    public void actualdailyUpdate(Connection conn,String currDT,float poa,float poaIradiance,float ghi,float ghiIradiance,float modTemp,float ambTemp,float acPower,float kwhTodayExport,float kwhTodayImport,float kwhTotalExport,float kwhTotalImport,float PRWeatherCorr,float NPR,float NOPR,float CPR,float COPR,float acCUF,float dcCUF,float CPA,String ID) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=null;
        updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_daily_actual_data SET update_date='%s',poa=%s,poa_iradiance=%s,ghi=%s,ghi_iradiance=%s,mod_temp=%s,amb_temp=%s,active_power=%s,kWh_today_export=%s,kWh_today_import=%s,kWh_total_export=%s,kWh_total_import=%s,PR_weather_curr=%s,NPR=%s,NOPR=%s,CPR=%s,COPR=%s,AC_CUF=%s,DC_CUF=%s,CPA=%s WHERE id=%s;", currDT,poa,poaIradiance,ghi,ghiIradiance,modTemp,ambTemp,acPower,kwhTodayExport,kwhTodayImport,kwhTotalExport,kwhTotalImport,PRWeatherCorr,NPR,NOPR,CPR,COPR,acCUF,dcCUF,CPA,ID);
        
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    
    public void actualdailyEntry(Connection conn,String plantID, String currDT,String todayD,String dateID,float poa,float poaIradiance,float ghi,float ghiIradiance,float modTemp,float ambTemp,float acPower,float kwhTodayExport,float kwhTodayImport,float kwhTotalExport,float kwhTotalImport,float PRWeatherCorr,float NPR,float NOPR,float CPR,float COPR,float acCUF,float dcCUF,float CPA) throws SQLException{
        Statement stmt= conn.createStatement();
        String entryQuery=null;
        entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_daily_actual_data (plant_id,update_date,today_date,today,dateid,poa,poa_iradiance,ghi,ghi_iradiance,mod_temp,amb_temp,active_power,kWh_today_export,kWh_today_import,kWh_total_export,kWh_total_import,PR_weather_curr,NPR,NOPR,CPR,COPR,AC_CUF,DC_CUF,CPA) VALUES ('%s','%s','%s','%s','%s',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", plantID,currDT,currDT,todayD,dateID,poa,poaIradiance,ghi,ghiIradiance,modTemp,ambTemp,acPower,kwhTodayExport,kwhTodayImport,kwhTotalExport,kwhTotalImport,PRWeatherCorr,NPR,NOPR,CPR,COPR,acCUF,dcCUF,CPA);
        
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    public void actualMonthlyUpdate(Connection conn, String currDT, double actualMonthlykWh, String pStatus, String ID) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=null;
        updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_monthly_actual_data SET update_date='%s',actual_kWh=%s,p_status='%s' WHERE id=%s;", currDT,actualMonthlykWh,pStatus,ID);
        
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    
    public void actualMonthlyEntry(Connection conn, String siteID, String currDT, String dateIDD,String dateID, double actualMonthlykWh, String pStatus) throws SQLException{
        Statement stmt= conn.createStatement();
        String entryQuery=null;
        entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_monthly_actual_data (plant_id,update_date,today_date,today,dateid,actual_kWh,p_status) VALUES ('%s','%s','%s','%s','%s',%s,'%s');", siteID,currDT,currDT,dateIDD,dateID,actualMonthlykWh,pStatus);
        
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
    public void actualYearlyUpdate(Connection conn, String currDT, double actualYearlykWh, String pStatus, String ID) throws SQLException{
        Statement stmt= conn.createStatement();
        String updateQuery=null;
        updateQuery=String.format("UPDATE swlflexi_CMSswlawsReMACS.Flexi_solar_yearly_actual_data SET update_date='%s',actual_kWh=%s,p_status='%s' WHERE id=%s;", currDT,actualYearlykWh,pStatus,ID);
        
        try{
            stmt.execute(updateQuery);
        }
        catch(Exception ex){
           System.out.println(updateQuery); 
        }
        stmt.close();
    }
    
    
    public void actualYearlyEntry(Connection conn, String siteID, String currDT, String dateIDD,String dateID, double actualYearlykWh, String pStatus) throws SQLException{
        Statement stmt= conn.createStatement();
        String entryQuery=null;
        entryQuery=String.format("INSERT INTO swlflexi_CMSswlawsReMACS.Flexi_solar_yearly_actual_data (plant_id,update_date,today_date,today,dateid,actual_kWh,p_status) VALUES ('%s','%s','%s','%s','%s',%s,'%s')", siteID,currDT,currDT,dateIDD,dateID,actualYearlykWh,pStatus);
        
        try{
            stmt.execute(entryQuery);
        }
        catch(Exception ex){
           System.out.println(entryQuery); 
        }
        stmt.close();
    }
    
}
    
    