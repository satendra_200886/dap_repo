
package com.sat.dap;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Aggregates;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import org.bson.Document;

public class dailyActual {
    sqlUpdateAndEntry sqlupdateandquery= new sqlUpdateAndEntry();
    getData getdata=new getData();
    
    public void dailyParam(List<Document> docListMongoLatest, String siteID, String siteZone, Connection conn, MongoCollection mongocollection, Instant todayStartDT) throws SQLException, ParseException{
        ZonedDateTime zoneddatetime=ZonedDateTime.now(ZoneId.of(siteZone));
        String currDT=zoneddatetime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String todayD=zoneddatetime.format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        String dateIDD=zoneddatetime.format(DateTimeFormatter.ofPattern("MMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat docFormat = new SimpleDateFormat("yyyy-MM-dd");
        String yesterdayD=zoneddatetime.minusDays(1).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        DateFormat DOCFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        Statement stmt4= conn.createStatement();
        Statement stmt5= conn.createStatement();
        Statement stmt6= conn.createStatement();
        Statement stmt7= conn.createStatement();
//        Statement stmt8= conn.createStatement();
        String wmsDailyQ=String.format("SELECT wlist.wms_id,wdaily.* FROM `Flexi_solar_wms_list` AS wlist JOIN `Flexi_solar_wms_daily` AS wdaily ON wdaily.plant_id = wlist.plant_id AND wdaily.wms_id=wlist.wms_id WHERE wlist.plant_id=%s AND wlist.WMS_Cumulative=1 AND wdaily.Today=%s order by wdaily.wms_id asc;", siteID, todayD);  
        String mfmDailyQ=String.format("SELECT mlist.M_ID,mdaily.* FROM `Flexi_solar_MFM_list` AS mlist JOIN `Flexi_solar_MFM_daily` AS mdaily ON mdaily.plant_id = mlist.plant_id AND mdaily.M_id=mlist.M_ID  WHERE mlist.plant_id=%s AND mlist.MFM_Cumulative=1 AND mdaily.Today=%s;", siteID, todayD);
        String actualDailyQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_daily_actual_data where plant_id=%s and today=%s;", siteID, todayD);  
        String plantInfoQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_plant_info where id=%s;", siteID);  
        String estimMonthQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_monthly_estimation_data where plant_id=%s;", siteID);  
        String estimDailyQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_daily_estimation_data where plant_id=%s;", siteID);  
        String wmsSensorEventQ=String.format("SELECT * FROM Flexi_solar_events WHERE plant_id =%s AND (start_today=%s or end_today=%s or end_today is null) AND error_code IN (8000,8001) group by device_name;",siteID, todayD, todayD);
//        String wmsCountQ=String.format("SELECT COUNT(id) FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s;",siteID);
        ResultSet wmsDailyRs=stmt1.executeQuery(wmsDailyQ);
        ResultSet mfmDailyRs=stmt2.executeQuery(mfmDailyQ);
        ResultSet actualDailyRs=stmt3.executeQuery(actualDailyQ);
        ResultSet plantInfoRs=stmt4.executeQuery(plantInfoQ);
        ResultSet estimMonthRs=stmt5.executeQuery(estimMonthQ);
        ResultSet estimDailyRs=stmt6.executeQuery(estimDailyQ);
        ResultSet wmsSensorEventRs=stmt7.executeQuery(wmsSensorEventQ);
//        ResultSet wmsCountRs=stmt8.executeQuery(wmsCountQ);
        
//        int wmsCount=1;
        float acCapacity=0.0f;
        float dcCapacity=0.0f;
        float plantLF=0.0f;
        float lineLF=0.0f;
        float modTempCoeff=0.0f;
        float plantNOCT=0.0f;
        Date DOC=null;
        float degradation=0.0f;
        float modTempEstDaily=0.0f;
        float modTempEstMonthly=0.0f;
        float poa=0.0f;
        float poaIradiance=0.0f;
        float poaIradianceCorr=0.0f;
        float ghi=0.0f;
        float ghiIradiance=0.0f;
        float modTemp=0.0f;
        float ambTemp=0.0f;
        float lossIradianceExtBd=0.0f;
        float modTempExcBd=0.0f;
        float tiltIrrAvgExcBD=0.0f;
        float kwhExpectedExcBd=0.0f;
        float acPower=0.0f;
        float kwhTodayExport=0.0f;
        float kwhTodayImport=0.0f;
        float kwhTotalExport=0.0f;
        float kwhTotalImport=0.0f;
        float estimtedPR=0.0f;
        float NPR=0.0f;
        float CPR=0.0f;
        float kwhTodayNet=0.0f;
        float NOPR=0.0f;
        float COPR=0.0f;
        float PA=0.0f;
        float CPA=0.0f;
        float PRWeatherCorr=0.0f;
        float dcCUF=0.0f;
        float acCUF=0.0f;
        float valueHolderF=0.0f;
        String valueHolderS="";
        int poaCo=0;
        int iradianceCo=0;
        int ghiCo=0;
        int ghiIradianceCo=0;
        int lossIradianceExtCo=0;
        int irrtiltavgExcCo=0;
        int modTempCo=0;
        int ambTempCo=0;
        int modTempExcBdCo=0;
        int kwhExpCo=0;
        float invWeight=0.0f;
        float smbWeight=0.0f;
        float stringWeight=0.0f;
        String dateID=null;
        boolean poaFail1=false;
        boolean poaFail2=false;
        boolean ghiFail1=false;
        boolean modTemFail1=false;
        boolean modTemFail2=false;
        boolean ambTemFail1=false;

        try{
            if(plantInfoRs.next()){
                valueHolderS=plantInfoRs.getString("plant_capacity");
                if(valueHolderS!=null){
                    acCapacity= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("plant_dc_capacity");
                if(valueHolderS!=null){
                    dcCapacity= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("loss_factor_plant_level");
                if(valueHolderS!=null){
                    plantLF= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("line_loss_factor");
                if(valueHolderS!=null){
                    lineLF= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("module_temp_coefficient");
                if(valueHolderS!=null){
                    modTempCoeff= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("plant_NOCT");
                if(valueHolderS!=null){
                    plantNOCT= Float.parseFloat(valueHolderS);
                }
                valueHolderS=plantInfoRs.getString("DOC");
                if(valueHolderS!=null){
                    DOC= DOCFormat.parse(valueHolderS);
                }
                long daysPassed= (dateFormat.parse(currDT).getTime() - DOC.getTime())/(24 * 60 * 60 * 1000);
                degradation= (float) ((daysPassed-365)*(0.7/36500));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

//        if(wmsCountRs.next()){
//            wmsCount=Integer.parseInt(wmsCountRs.getString("COUNT(id)"));
//        }
        
        try{
            while(estimMonthRs.next()){
                if(estimMonthRs.getString("dateid").contentEquals(siteID+"-"+dateIDD)){
                    valueHolderS=estimMonthRs.getString("PR_guar");
                    if(valueHolderS!=null){
                        estimtedPR= Float.parseFloat(valueHolderS);
                    }
                    valueHolderS=estimMonthRs.getString("mod_temp_est");
                    if(valueHolderS!=null){
                        modTempEstMonthly= Float.parseFloat(valueHolderS);
                    }
                    break;
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        try{
            while(estimDailyRs.next()){
                if(estimDailyRs.getString("dateid").contentEquals(siteID+"-"+todayD)){
                    modTempEstDaily=Float.parseFloat(estimDailyRs.getString("module_temp_avg_pvsys"));
                    break;
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        while(wmsDailyRs.next()){
            poaFail1=false;
            poaFail2=false;
            ghiFail1=false;
            modTemFail1=false;
            modTemFail2=false;
            ambTemFail1=false;
            
            wmsSensorEventRs.beforeFirst();
            while(wmsSensorEventRs.next()){
                if(wmsDailyRs.getString("wms_id").contentEquals(wmsSensorEventRs.getString("device_id"))){
                    if(wmsSensorEventRs.getString("device_name").endsWith("POA-1")){
                        poaFail1=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("POA-2")){
                        poaFail2=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("Mod-Temp1")){
                        modTemFail1=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("Mod-Temp2")){
                        modTemFail2=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("GHI-1")){
                        ghiFail1=true;
                    }
                    else if(wmsSensorEventRs.getString("device_name").endsWith("Amb-Temp")){
                        ambTemFail1=true;
                    }
                    break;
                }
            }
            
            try{
                if(!poaFail1){
                    valueHolderF=Float.parseFloat(wmsDailyRs.getString("irradiation_tilt1"));
                    if(valueHolderF>0){
                        poa=poa+valueHolderF;
                        poaCo++;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(!poaFail2){
                    valueHolderF=Float.parseFloat(wmsDailyRs.getString("irradiation_tilt2"));
                    if(valueHolderF>0){
                        poa=poa+valueHolderF;
                        poaCo++;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                valueHolderF=Float.parseFloat(wmsDailyRs.getString("poa_irr_eve"));
                if(valueHolderF>0){
                    poaIradiance=poaIradiance+valueHolderF;
                    iradianceCo++;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(!ghiFail1){
                    valueHolderF=Float.parseFloat(wmsDailyRs.getString("gloabal_irradiation"));
                    if(valueHolderF>0){
                        ghi=ghi+valueHolderF;
                        ghiCo++;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                valueHolderF=Float.parseFloat(wmsDailyRs.getString("ghi_irr_eve"));
                if(valueHolderF>0){
                    ghiIradiance=ghiIradiance+valueHolderF;
                    ghiIradianceCo++;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(!modTemFail1){
                    valueHolderF=Float.parseFloat(wmsDailyRs.getString("module1_tmp"));
                    if(valueHolderF>0){
                        modTemp=modTemp+valueHolderF;
                        modTempCo++;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(!modTemFail2){
                    valueHolderF=Float.parseFloat(wmsDailyRs.getString("module2_tmp"));
                    if(valueHolderF>0){
                        modTemp=modTemp+valueHolderF;
                        modTempCo++;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                if(!ambTemFail1){
                    valueHolderF=Float.parseFloat(wmsDailyRs.getString("ambient_tmp"));
                    if(valueHolderF>0){
                        ambTemp=ambTemp+valueHolderF;
                        ambTempCo++;
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                valueHolderF=Float.parseFloat(wmsDailyRs.getString("mod_tmpavg_exc_bd"));
                if(valueHolderF>0){
                    modTempExcBd=modTempExcBd+valueHolderF;
                    modTempExcBdCo++;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                valueHolderF=Float.parseFloat(wmsDailyRs.getString("loss_irradiance_ext_bd"));
                if(valueHolderF>0){
                    lossIradianceExtBd=lossIradianceExtBd+valueHolderF;
                    lossIradianceExtCo++;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                valueHolderF=Float.parseFloat(wmsDailyRs.getString("irr_tiltavg_exc_bd"));
                if(valueHolderF>0){
                    tiltIrrAvgExcBD=tiltIrrAvgExcBD+valueHolderF;
                    irrtiltavgExcCo++;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                valueHolderF=Float.parseFloat(wmsDailyRs.getString("expected_energy"));
                if(valueHolderF>0){
                    kwhExpectedExcBd=kwhExpectedExcBd+valueHolderF;
                    kwhExpCo++;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        wmsDailyRs.beforeFirst();
        if(poaCo>0){
            poa=poa/poaCo;
        }
        if(iradianceCo>0){
            poaIradiance=poaIradiance/iradianceCo;
        }
        if(ghiCo>0){
            ghi=ghi/ghiCo;
        }
        if(ghiIradianceCo>0){
            ghiIradiance=ghiIradiance/ghiIradianceCo;
        }
        if(lossIradianceExtCo>0){
            lossIradianceExtBd=lossIradianceExtBd/lossIradianceExtCo;
        }
        if(irrtiltavgExcCo>0){
            tiltIrrAvgExcBD=tiltIrrAvgExcBD/irrtiltavgExcCo;
        }
        if(modTempCo>0){
            modTemp=modTemp/modTempCo;
        }
        if(ambTempCo>0){
            ambTemp=ambTemp/ambTempCo;
        }
        if(modTempExcBdCo>0){
            modTempExcBd=modTempExcBd/modTempExcBdCo;
        }
        if(kwhExpCo>0){
            kwhExpectedExcBd=kwhExpectedExcBd/kwhExpCo;
        }

        while(mfmDailyRs.next()){
            try{
                valueHolderF=Float.parseFloat(mfmDailyRs.getString("AC_Power"));
                acPower=acPower+valueHolderF;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            try{
                valueHolderF=Float.parseFloat(mfmDailyRs.getString("Energy_Export_today"));
                kwhTodayExport=kwhTodayExport+valueHolderF;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            try{
                valueHolderF=Float.parseFloat(mfmDailyRs.getString("Energy_import_today"));
                kwhTodayImport=kwhTodayImport+valueHolderF;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                valueHolderF=Float.parseFloat(mfmDailyRs.getString("Energy_Export_total"));
                kwhTotalExport=kwhTotalExport+valueHolderF;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            
            try{
                valueHolderF=Float.parseFloat(mfmDailyRs.getString("Energy_import_total"));
                kwhTotalImport=kwhTotalImport+valueHolderF;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            try{
                valueHolderF=Float.parseFloat(mfmDailyRs.getString("MFM_PR"));
                NPR=NPR+valueHolderF;
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        try{
            kwhTodayNet=kwhTodayExport-kwhTodayImport;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        try{
            if(kwhExpectedExcBd>0.0){
                NOPR=(kwhTodayExport/kwhExpectedExcBd)*100;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        try{
            if(acCapacity>0.0){
                acCUF= (kwhTodayExport/(acCapacity*24))*100;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        try{
            if(dcCapacity>0.0){
                dcCUF= (kwhTodayExport/(dcCapacity*24))*100;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        try{
            if(dcCapacity>0.0 && poaIradiance>0.0){
                PRWeatherCorr= (kwhTodayExport/(dcCapacity*(poaIradiance-lossIradianceExtBd)*(1-(modTempCoeff*(modTempExcBd-modTempEstDaily)))))*100;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        
        if(siteID.contentEquals("137")){
            CPR=fifteenMinGetPR(siteID,siteZone,conn,mongocollection,todayStartDT,dcCapacity,modTempCoeff,modTempEstMonthly);
            if(estimtedPR>0 && CPR>0.0){
                COPR=(CPR/estimtedPR)*100;
            }
        }
        else if(siteID.contentEquals("41") || siteID.contentEquals("42")){
            CPR=oneHourGetPR(siteID,siteZone,conn,mongocollection,todayStartDT,dcCapacity,modTempCoeff,modTempEstMonthly);
            if(estimtedPR>0 && CPR>0.0){
                COPR=(CPR/estimtedPR)*100;
            }
        }
        else if(siteID.contentEquals("141") || siteID.contentEquals("142")){
            CPR=tenMinGetPR(siteID,siteZone,conn,mongocollection,todayStartDT,dcCapacity,modTempCoeff,modTempEstMonthly,lineLF);
            if(estimtedPR>0 && CPR>0.0){
                COPR=(CPR/estimtedPR)*100;
            }
        }
        else if(siteID.contentEquals("11") || siteID.contentEquals("19") || siteID.contentEquals("20")){
            CPR=NPR;
            if(estimtedPR>0 && CPR>0.0){
                COPR=(CPR/estimtedPR)*100;
            }
        }
        else{
            try{
                if(poaIradiance>0.0 && dcCapacity>0.0){
                    CPR=((kwhTodayExport*1000)/(((poaIradiance-lossIradianceExtBd)*dcCapacity*1000)*(1-(((modTempExcBd+(tiltIrrAvgExcBD/1000)*3)-plantNOCT)*modTempCoeff))))*100;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        
            try{
                if(estimtedPR>0 && CPR>0.0){
                    COPR=(CPR/estimtedPR)*100;
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if(siteID.contentEquals("19") || siteID.contentEquals("20") || siteID.contentEquals("21") || siteID.contentEquals("39") || siteID.contentEquals("141") || siteID.contentEquals("142") || siteID.contentEquals("41") || siteID.contentEquals("42") || siteID.contentEquals("137")){
            if(siteID.contentEquals("19")){
                invWeight=14.29f;
                smbWeight=2.38f;
                stringWeight=0.11f;
            }
            else if(siteID.contentEquals("20")){
                invWeight=7.14f;
                smbWeight=1.19f;
                stringWeight=0.05f;
            }
            else if(siteID.contentEquals("21")){
                invWeight=5.0f;
                smbWeight=0.71f;
                stringWeight=0.06f;
            }
            else if(siteID.contentEquals("39")){
                invWeight=2.5f;
                smbWeight=0.28f;
                stringWeight=0.02f;
            }
            else if(siteID.contentEquals("141")){
                invWeight=5.0f;
                smbWeight=0.23f;
                stringWeight=0.02f;
            }
            else if(siteID.contentEquals("142")){
                invWeight=5.0f;
                smbWeight=0.23f;
                stringWeight=0.02f;
            }
            else if(siteID.contentEquals("41")){
                invWeight=0.08613f;
                stringWeight=0.01f;
            }
            else if(siteID.contentEquals("42")){
                invWeight=0.085985f;
                stringWeight=0.02f;
            }
            else if(siteID.contentEquals("137")){
                invWeight=4.545454545f;
                stringWeight=0.04f;
            }
            CPA=cPA1(siteID, siteZone, conn, dcCapacity, invWeight, smbWeight, stringWeight);
        }
        if(siteID.contentEquals("11")){
            CPA=cPA2(estimtedPR, kwhTodayExport, siteID, siteZone, mongocollection, conn);
        }

        dateID=siteID+"-"+todayD;
        boolean match=false;
        actualDailyRs.beforeFirst();
        while(actualDailyRs.next()){
            if((actualDailyRs.getString("dateid")).contentEquals(dateID)){
                String ID=actualDailyRs.getString("id");
                sqlupdateandquery.actualdailyUpdate(conn,currDT,poa,poaIradiance,ghi,ghiIradiance,modTemp,ambTemp,acPower,kwhTodayExport,kwhTodayImport,kwhTotalExport,kwhTotalImport,PRWeatherCorr,NPR,NOPR,CPR,COPR,acCUF,dcCUF,CPA,ID);
                match=true;
                break;
            }
        }
        if(!match){
            sqlupdateandquery.actualdailyEntry(conn,siteID,currDT,todayD,dateID,poa,poaIradiance,ghi,ghiIradiance,modTemp,ambTemp,acPower,kwhTodayExport,kwhTodayImport,kwhTotalExport,kwhTotalImport,PRWeatherCorr,NPR,NOPR,CPR,COPR,acCUF,dcCUF,CPA);
        }

        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
            stmt4.close();
            stmt5.close();
            stmt6.close();
            stmt7.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }  

    }
    
    
    public float tenMinGetPR(String siteID, String siteZone, Connection conn, MongoCollection mongocollection, Instant todayStartDT, float dcCapacity, float modTempCoeff, float modTempEstMonthly, float lineLF) throws SQLException, ParseException {
        DateFormat formatoutput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateFormat formatinput = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        String wmsListQ=String.format("SELECT wms_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s and WMS_Cumulative=1;", siteID);  
        String mfmListQ=String.format("SELECT M_ID FROM swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_list where plant_id=%s and MFM_Cumulative=1;", siteID);
        ResultSet wmsListRs=stmt1.executeQuery(wmsListQ);
        ResultSet mfmListRs=stmt2.executeQuery(mfmListQ);

        List<String> wmsList=new ArrayList<>();
        List<String> mfmList=new ArrayList<>();

        while(wmsListRs.next()){
            wmsList.add(siteID+"_WMS_"+wmsListRs.getString("wms_id"));
        }
        while(mfmListRs.next()){
            mfmList.add(siteID+"_MFM_"+mfmListRs.getString("M_ID"));
        }
        
        List<List<Document>> wmsDocMongoLL=new ArrayList<>();
        List<List<Document>> mfmDocMongoLL=new ArrayList<>();
        List<List<Document>> wmsDoc10MinLL=new ArrayList<>();
        List<List<Document>> mfmDoc10MinLL=new ArrayList<>();
        
        for(String wmsDev:wmsList){
            wmsDocMongoLL.add(getdata.mongoDevGteData1(wmsDev, todayStartDT, mongocollection, conn, siteID));
        }
        
        for(String mfmDev:mfmList){
            mfmDocMongoLL.add(getdata.mongoDevGteData1(mfmDev, todayStartDT, mongocollection, conn, siteID));
        }
        
        //for actual calculation of PR
        float poa=0.0f;
        float modtemp=0.0f;
        int poaC=0;
        int modtempC=0;
        float activePow=0.0f;
        float cpr=100f;
        int cprC=0;
        
        //for WMS
        Set keyset=null;
        float w21=0.0f;
        float w22=0.0f;
        float w10=0.0f;
        float w14=0.0f;
        float w21w22avg=0.0f;
        float w10w14avg=0.0f;
        int w21c=0;
        int w22c=0;
        int w10c=0;
        int w14c=0;
        int sampleC=0;
        float valueHolderF=0.0f;
        Date newdate;
        Document tenMinAvgDoc=null;
        List<Document> wmsDoc10MinL=new ArrayList<>();
        for(List<Document> wmsDocMongoL:wmsDocMongoLL){
            wmsDoc10MinL=new ArrayList<>();
            for(Document wmsDoc:wmsDocMongoL){
                String device=wmsDoc.getString("device");
                keyset=wmsDoc.keySet();
                newdate=wmsDoc.getDate("ts");
                ZonedDateTime date=newdate.toInstant().atZone(ZoneId.of(siteZone));
                if(!(date.getHour()==0 && date.getMinute()==0)){
                    if((date.getMinute()%10)==0){
                        if(w21c>0){
                            w21=w21/w21c;
                        }
                        if(w22c>0){
                            w22=w22/w22c;
                        }
                        if(w10c>0){
                            w10=w10/w10c;
                        }
                        if(w14c>0){
                            w14=w14/w14c;
                        }

                        if(w22>0.0){
                            w21w22avg=(w21+w22)/2;
                        }
                        else{
                            w21w22avg=w21;
                        }
                        
                        if(w14>0.0){
                            w10w14avg=(w10+w14)/2;
                        }
                        else{
                            w10w14avg=w10;
                        }
                        
                        if((w21w22avg/6)>2 && sampleC==10){
                            //compaerision will be on date basis will not work on zoneddatetime
                            newdate.setSeconds(0);
                            tenMinAvgDoc = new Document("date", newdate)
                              .append("device", device)
                              .append("poa", w21w22avg)
                              .append("modtemp", w10w14avg); 
                            
                            wmsDoc10MinL.add(tenMinAvgDoc);
                        }

                        w21=0.0f;
                        w22=0.0f;
                        w10=0.0f;
                        w14=0.0f;
                        w21c=0;
                        w22c=0;
                        w10c=0;
                        w14c=0;
                        sampleC=0;
                    }
                }
            
                try{
                    if(keyset.contains("w21")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w21").toString());
                        if(!Float.isNaN(valueHolderF)){
                            if(valueHolderF>0){
                                w21=w21+valueHolderF;
                                w21c++;
                            }
                            else{
                                continue;
                            }
                        }
                        else{
                            continue;
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch w21");
                }

                try{
                    if(keyset.contains("w22")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w22").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w22=w22+valueHolderF;
                            w22c++;
                        }
                    } 
                }
                catch (Exception ex) {
                    System.out.println("catch w22");
                }
                
                try{
                    if(keyset.contains("w10")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w10").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w10=w10+valueHolderF;
                            w10c++;
                        }
                    } 
                }
                catch (Exception ex) {
                    System.out.println("catch w10");
                }
                
                try{
                    if(keyset.contains("w14")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w14").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w14=w14+valueHolderF;
                            w14c++;
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch w14");
                }
                sampleC++;
            }
            wmsDoc10MinLL.add(wmsDoc10MinL);
        }
        List<Date> wmsStartDateL=new ArrayList<>();
        Date wmsStartDate=new Date();
        for(List<Document> listd:wmsDoc10MinLL){
            for(Document d:listd){
                wmsStartDateL.add(d.getDate("date"));
                break;
            }
        }

        for(Date d:wmsStartDateL){
            if(d.compareTo(wmsStartDate)<0){
                wmsStartDate=d;
            }
        }
        wmsStartDate.setSeconds(0);
        
        //for MFM
        float m32=0.0f;
        int m32c=0;
        List<Document> mfmDoc10MinL=new ArrayList<>();
        for(List<Document> mfmDocMongoL:mfmDocMongoLL){
            mfmDoc10MinL=new ArrayList<>();
            for(Document mfmDoc:mfmDocMongoL){
                String device=mfmDoc.getString("device");
                keyset=mfmDoc.keySet();
                newdate=mfmDoc.getDate("ts");
                ZonedDateTime date=newdate.toInstant().atZone(ZoneId.of(siteZone));
                if(!(date.getHour()==0 && date.getMinute()==0)){
                    if((date.getMinute()%10)==0){
//                        if(m32>0){
//                            m32=m32/m32c;
//                        }
                        //m32c is for only understanding th ecount if energy lower
                        if((mfmDoc.getDate("ts").compareTo(wmsStartDate)>=0) && sampleC==10){
                            //compaerision will be on date basis will not work on zoneddatetime
                            newdate.setSeconds(0);
                            tenMinAvgDoc = new Document("date", newdate)
                              .append("device", device)
                              .append("count", sampleC)
                              .append("m32c", m32c)
                              .append("m32", m32);
                            
                            mfmDoc10MinL.add(tenMinAvgDoc);
                        }

                        m32=0.0f;
                        m32c=0;
                        sampleC=0;
                    }
                }
            
                try{
                    if(keyset.contains("m32")){
                        valueHolderF=Float.parseFloat(mfmDoc.get("m32").toString());
                        if(!Float.isNaN(valueHolderF)){
                            m32=m32+valueHolderF;
                            m32c++;
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch m32");
                }
                sampleC++;
            }
            mfmDoc10MinLL.add(mfmDoc10MinL);
        }
        
        List<Float> CPR=new ArrayList<>();
        for(Document d:wmsDoc10MinLL.get(0)){
            poa=0.0f;
            modtemp=0.0f;
            poaC=0;
            modtempC=0;
            activePow=0.0f;
            cpr=0.0f;
            cprC=0;
            newdate=d.getDate("date");
            try{
                poa=Float.parseFloat(d.get("poa").toString());
                poaC++;
            }
            catch (Exception ex) {
                System.out.println("catch actualestimation 10minpr poa");
            }
            try{
                modtemp=Float.parseFloat(d.get("modtemp").toString());
                modtempC++;
            }
            catch (Exception ex) {
                System.out.println("catch actualestimation 10minpr modtemp");
            }
            
            for(int i=1;i<wmsDoc10MinLL.size();i++){
                for(Document d2:wmsDoc10MinLL.get(i)){
                    if(d2.getDate("date").getHours()==newdate.getHours() && d2.getDate("date").getMinutes()==newdate.getMinutes()){
                        try{
                            poa=poa+Float.parseFloat(d2.get("poa").toString());
                            poaC++;
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 10minpr w21w22avg");
                        }
                        try{
                            modtemp=modtemp+Float.parseFloat(d2.get("modtemp").toString());
                            modtempC++;
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 10minpr w10w14avg");
                        }
                        break;
                    }
                }
            }
            
            for(int i=0;i<mfmDoc10MinLL.size();i++){
                for(Document d3:mfmDoc10MinLL.get(i)){
                    if(d3.getDate("date").getHours()==newdate.getHours() && d3.getDate("date").getMinutes()==newdate.getMinutes()){
                        try{
                            valueHolderF=Float.parseFloat(d3.get("m32").toString());
                            if(!Float.isNaN(valueHolderF)){
                                activePow=activePow+valueHolderF;
                            }
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 10minpr m32");
                        }
                        break;
                    }
                }
            }
            
            if(dcCapacity>0 && poa>0 && lineLF>0){
                cpr=cpr+((activePow/60)/(dcCapacity*(poa/(poaC*6000))*(1-modTempCoeff*(modTempEstMonthly-(modtemp/modtempC))))*lineLF)*100;
                cprC++;
            }
        }

        if(cprC>1){
            cpr=cpr/cprC;
        }
        try{
            stmt1.close();
            stmt2.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }
        return cpr;
    }
    
    
    public float fifteenMinGetPR(String siteID, String siteZone, Connection conn, MongoCollection mongocollection, Instant todayStartDT, float dcCapacity, float modTempCoeff, float modTempEstMonthly) throws SQLException, ParseException {
        DateFormat formatoutput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateFormat formatinput = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        String wmsListQ=String.format("SELECT wms_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s and WMS_Cumulative=1;", siteID);  
        String mfmListQ=String.format("SELECT M_ID FROM swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_list where plant_id=%s and MFM_Cumulative=1;", siteID);
        ResultSet wmsListRs=stmt1.executeQuery(wmsListQ);
        ResultSet mfmListRs=stmt2.executeQuery(mfmListQ);

        List<String> wmsList=new ArrayList<>();
        List<String> mfmList=new ArrayList<>();

        while(wmsListRs.next()){
            wmsList.add(siteID+"_WMS_"+wmsListRs.getString("wms_id"));
        }
        while(mfmListRs.next()){
            mfmList.add(siteID+"_MFM_"+mfmListRs.getString("M_ID"));
        }
        
        List<List<Document>> wmsDocMongoLL=new ArrayList<>();
        List<List<Document>> mfmDocMongoLL=new ArrayList<>();
        List<List<Document>> wmsDoc10MinLL=new ArrayList<>();
        List<List<Document>> mfmDoc10MinLL=new ArrayList<>();
        
        for(String wmsDev:wmsList){
            wmsDocMongoLL.add(getdata.mongoDevGteData1(wmsDev, todayStartDT, mongocollection, conn, siteID));
        }
        
        for(String mfmDev:mfmList){
            mfmDocMongoLL.add(getdata.mongoDevGteData1(mfmDev, todayStartDT, mongocollection, conn, siteID));
        }
        
        //for actual calculation of PR
        float poa=0.0f;
        float modtemp=0.0f;
        int poaC=0;
        int modtempC=0;
        float activePow=0.0f;
        float cpr=100f;
        int cprC=0;
        
        //for WMS
        Set keyset=null;
        float w21=0.0f;
        float w22=0.0f;
        float w10=0.0f;
        float w14=0.0f;
        float w21w22avg=0.0f;
        float w10w14avg=0.0f;
        int w21c=0;
        int w22c=0;
        int w10c=0;
        int w14c=0;
        int sampleC=0;
        float valueHolderF=0.0f;
        float valueHolderF1=0.0f;
        Date newdate;
        Document tenMinAvgDoc=null;
        List<Document> wmsDoc10MinL=new ArrayList<>();
        for(List<Document> wmsDocMongoL:wmsDocMongoLL){
            wmsDoc10MinL=new ArrayList<>();
            for(Document wmsDoc:wmsDocMongoL){
                String device=wmsDoc.getString("device");
                keyset=wmsDoc.keySet();
                newdate=wmsDoc.getDate("ts");
                ZonedDateTime date=newdate.toInstant().atZone(ZoneId.of(siteZone));
                if(!(date.getHour()==0 && date.getMinute()==0)){
                    if((date.getMinute()%15)==0){
                        if(w21c>0){
                            w21=w21/w21c;
                        }
                        if(w22c>0){
                            w22=w22/w22c;
                        }
                        if(w10c>0){
                            w10=w10/w10c;
                        }
                        if(w14c>0){
                            w14=w14/w14c;
                        }

                        if(w22>0.0){
                            w21w22avg=(w21+w22)/2;
                        }
                        else{
                            w21w22avg=w21;
                        }
                        
                        if(w14>0.0){
                            w10w14avg=(w10+w14)/2;
                        }
                        else{
                            w10w14avg=w10;
                        }
                        
                        if((w21w22avg/4)>5 && sampleC==15){
                            //compaerision will be on date basis will not work on zoneddatetime
                            newdate.setSeconds(0);
                            tenMinAvgDoc = new Document("date", newdate)
                              .append("device", device)
                              .append("poa", w21w22avg)
                              .append("modtemp", w10w14avg); 
                            
                            wmsDoc10MinL.add(tenMinAvgDoc);
                        }

                        w21=0.0f;
                        w22=0.0f;
                        w10=0.0f;
                        w14=0.0f;
                        w21c=0;
                        w22c=0;
                        w10c=0;
                        w14c=0;
                        sampleC=0;
                    }
                }
            
                try{
                    if(keyset.contains("w21")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w21").toString());
                        if(!Float.isNaN(valueHolderF)){
                            if(valueHolderF>0){
                                w21=w21+valueHolderF;
                                w21c++;
                            }
                            else{
                                continue;
                            }
                        }
                        else{
                            continue;
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch w21");
                }

                try{
                    if(keyset.contains("w22")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w22").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w22=w22+valueHolderF;
                            w22c++;
                        }
                    } 
                }
                catch (Exception ex) {
                    System.out.println("catch w22");
                }
                
                try{
                    if(keyset.contains("w10")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w10").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w10=w10+valueHolderF;
                            w10c++;
                        }
                    } 
                }
                catch (Exception ex) {
                    System.out.println("catch w10");
                }
                
                try{
                    if(keyset.contains("w14")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w14").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w14=w14+valueHolderF;
                            w14c++;
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch w14");
                }
                sampleC++;
            }
            wmsDoc10MinLL.add(wmsDoc10MinL);
        }

        List<Date> wmsStartDateL=new ArrayList<>();
        Date wmsStartDate=new Date();
        for(List<Document> listd:wmsDoc10MinLL){
            for(Document d:listd){
                wmsStartDateL.add(d.getDate("date"));
                break;
            }
        }

        for(Date d:wmsStartDateL){
            if(d.compareTo(wmsStartDate)<0){
                wmsStartDate=d;
            }
        }
        wmsStartDate.setSeconds(0);
        
        //for MFM
        float expEner=0.0f;
        float impEner=0.0f;
        float netEner15=0.0f;
        float netEnerLatest=0.0f;
        float netEnerLast=0.0f;
        int netEnerC=0;
        List<Document> mfmDoc10MinL=new ArrayList<>();
        for(List<Document> mfmDocMongoL:mfmDocMongoLL){
            mfmDoc10MinL=new ArrayList<>();
            for(Document mfmDoc:mfmDocMongoL){
                String device=mfmDoc.getString("device");
                keyset=mfmDoc.keySet();
                newdate=mfmDoc.getDate("ts");
                ZonedDateTime date=newdate.toInstant().atZone(ZoneId.of(siteZone));
                if(!(date.getHour()==0 && date.getMinute()==0)){
                    if((date.getMinute()%15)==0){
                        netEner15=netEnerLatest-netEnerLast;
                        netEnerLast=netEnerLatest;
                        if((mfmDoc.getDate("ts").compareTo(wmsStartDate)>=0) && sampleC==15){
                            //compaerision will be on date basis will not work on zoneddatetime
                            newdate.setSeconds(0);
                            tenMinAvgDoc = new Document("date", newdate)
                              .append("device", device)
                              .append("count", sampleC)
                              .append("netEnerC", netEnerC)    
                              .append("netEner15", netEner15);
                            
                            mfmDoc10MinL.add(tenMinAvgDoc);
                        }

                        netEner15=0.0f;
                        netEnerC=0;
                        sampleC=0;
                    }
                }
            
                try{
                    if(keyset.contains("m63") && keyset.contains("m92")){
                        valueHolderF=Float.parseFloat(mfmDoc.get("m63").toString());
                        valueHolderF1=Float.parseFloat(mfmDoc.get("m92").toString());
                        if(!Float.isNaN(valueHolderF) && !Float.isNaN(valueHolderF1)){
                            netEnerLatest=valueHolderF-valueHolderF1;
                        }
                        netEnerC++;
                    }
                    else if(keyset.contains("m66") && keyset.contains("m65")){
                        valueHolderF=Float.parseFloat(mfmDoc.get("m66").toString());
                        valueHolderF1=Float.parseFloat(mfmDoc.get("m65").toString());
                        if(!Float.isNaN(valueHolderF) && !Float.isNaN(valueHolderF1)){
                            netEnerLatest=valueHolderF-valueHolderF1;
                        }
                        netEnerC++;
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch netEner");
                }
                sampleC++;
            }
            mfmDoc10MinLL.add(mfmDoc10MinL);
        }
        
        List<Float> CPR=new ArrayList<>();
        for(Document d:wmsDoc10MinLL.get(0)){
            poa=0.0f;
            modtemp=0.0f;
            poaC=0;
            modtempC=0;
            activePow=0.0f;
            cpr=0.0f;
            cprC=0;
            newdate=d.getDate("date");
            try{
                poa=Float.parseFloat(d.get("poa").toString());
                poaC++;
            }
            catch (Exception ex) {
                System.out.println("catch actualestimation 15minpr poa");
            }
            try{
                modtemp=Float.parseFloat(d.get("modtemp").toString());
                modtempC++;
            }
            catch (Exception ex) {
                System.out.println("catch actualestimation 15minpr modtemp");
            }
            
            for(int i=1;i<wmsDoc10MinLL.size();i++){
                for(Document d2:wmsDoc10MinLL.get(i)){
                  if(d2.getDate("date").getHours()==newdate.getHours() && d2.getDate("date").getMinutes()==newdate.getMinutes()){
                        try{
                            poa=poa+Float.parseFloat(d2.get("poa").toString());
                            poaC++;
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 10minpr w21w22avg");
                        }
                        try{
                            modtemp=modtemp+Float.parseFloat(d2.get("modtemp").toString());
                            modtempC++;
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 15minpr w10w14avg");
                        }
                        break;
                    }
                }
            }
            
            for(int i=0;i<mfmDoc10MinLL.size();i++){
                for(Document d3:mfmDoc10MinLL.get(i)){
                    if(d3.getDate("date").getHours()==newdate.getHours() && d3.getDate("date").getMinutes()==newdate.getMinutes()){
                        try{
                            valueHolderF=Float.parseFloat(d3.get("netEner15").toString());
                            if(!Float.isNaN(valueHolderF)){
                                netEner15=netEner15+valueHolderF;
                            }
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 15minpr netEner15");
                        }
                        break;
                    }
                }
            }
            if(dcCapacity>0 && poa>0){
                cpr=cpr+(netEner15/(dcCapacity*(poa/(poaC*6000))*(1-modTempCoeff*(modTempEstMonthly-(modtemp/modtempC)))))*100;
                cprC++;
            }
        }
        if(cprC>1){
            cpr=cpr/cprC;
        }
        try{
            stmt1.close();
            stmt2.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }
        return cpr;
    }
    
    
    public float oneHourGetPR(String siteID, String siteZone, Connection conn, MongoCollection mongocollection, Instant todayStartDT, float dcCapacity, float modTempCoeff, float modTempEstMonthly) throws SQLException, ParseException {
        DateFormat formatoutput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateFormat formatinput = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        DateFormat formatsql = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateidDate=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        Statement stmt3= conn.createStatement();
        String wmsListQ=String.format("SELECT wms_id FROM swlflexi_CMSswlawsReMACS.Flexi_solar_wms_list where plant_id=%s and WMS_Cumulative=1;", siteID);  
        String mfmListQ=String.format("SELECT M_ID FROM swlflexi_CMSswlawsReMACS.Flexi_solar_MFM_list where plant_id=%s and MFM_Cumulative=1;", siteID);
        String estimHourlyQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_hourly_estimation_data where plant_id=%s and date_time>='%s 00' and date_time<='%s 24' order by date_time;", siteID, dateidDate, dateidDate);  
        
        ResultSet wmsListRs=stmt1.executeQuery(wmsListQ);
        ResultSet mfmListRs=stmt2.executeQuery(mfmListQ);
        ResultSet estimHourlyRs=stmt3.executeQuery(estimHourlyQ);
        
        //making the list for 24 hour each with hourly data for respective
        List<Float> estimModTempHourlyList=new ArrayList<>();
        for(int t=0; t<24;t++){
            estimHourlyRs.next();
            if(formatsql.parse(estimHourlyRs.getString("date_time")).getHours()==t){
                estimModTempHourlyList.add(Float.parseFloat(estimHourlyRs.getString("module_temp_avg_pvsys")));
            }
            else{
                estimModTempHourlyList.add(0.0f);
            }
        }

        List<String> wmsList=new ArrayList<>();
        List<String> mfmList=new ArrayList<>();

        while(wmsListRs.next()){
            wmsList.add(siteID+"_WMS_"+wmsListRs.getString("wms_id"));
        }
        while(mfmListRs.next()){
            mfmList.add(siteID+"_MFM_"+mfmListRs.getString("M_ID"));
        }
        
        List<List<Document>> wmsDocMongoLL=new ArrayList<>();
        List<List<Document>> mfmDocMongoLL=new ArrayList<>();
        List<List<Document>> wmsDoc10MinLL=new ArrayList<>();
        List<List<Document>> mfmDoc10MinLL=new ArrayList<>();
        
        for(String wmsDev:wmsList){
            wmsDocMongoLL.add(getdata.mongoDevGteData1(wmsDev, todayStartDT, mongocollection, conn, siteID));
        }
        
        for(String mfmDev:mfmList){
            mfmDocMongoLL.add(getdata.mongoDevGteData1(mfmDev, todayStartDT, mongocollection, conn, siteID));
        }
        
        //for actual calculation of PR
        float poa=0.0f;
        float modtemp=0.0f;
        int poaC=0;
        int modtempC=0;
        float activePow=0.0f;
        float cpr=100f;
        int cprC=0;
        float estimModTempHourly=0.0f;
        
        //for WMS
        Set keyset=null;
        float w21=0.0f;
        float w22=0.0f;
        float w10=0.0f;
        float w14=0.0f;
        float w21w22avg=0.0f;
        float w10w14avg=0.0f;
        int w21c=0;
        int w22c=0;
        int w10c=0;
        int w14c=0;
        int sampleC=0;
        float valueHolderF=0.0f;
        boolean cprCondition=false;
        int cprConCou=0;
        int hourN=0;
        Date newdate;
        Document tenMinAvgDoc=null;
        List<Document> wmsDoc10MinL=new ArrayList<>();
        for(List<Document> wmsDocMongoL:wmsDocMongoLL){
            wmsDoc10MinL=new ArrayList<>();
            for(Document wmsDoc:wmsDocMongoL){
                String device=wmsDoc.getString("device");
                keyset=wmsDoc.keySet();
                newdate=wmsDoc.getDate("ts");
                ZonedDateTime date=newdate.toInstant().atZone(ZoneId.of(siteZone));
                if(!(date.getHour()==0 && date.getMinute()==0)){
                    if((date.getMinute()%60)==0){
                        if(w21c>0){
                            w21=w21/w21c;
                        }
                        if(w22c>0){
                            w22=w22/w22c;
                        }
                        if(w10c>0){
                            w10=w10/w10c;
                        }
                        if(w14c>0){
                            w14=w14/w14c;
                        }

                        if(w22>0.0){
                            w21w22avg=(w21+w22)/2;
                        }
                        else{
                            w21w22avg=w21;
                        }
                        
                        if(w14>0.0){
                            w10w14avg=(w10+w14)/2;
                        }
                        else{
                            w10w14avg=w10;
                        }
                        
                        if(w21w22avg>600 && hourN+1==date.getMinute()){
                            hourN=date.getMinute();
                            cprConCou++;
                        }
                        else{
                            cprConCou=0;
                        }
                        
                        if(cprConCou>=3){
                            cprCondition=true;
                        }
                        
                        if((w21w22avg)>300 && sampleC==60){
                            //compaerision will be on date basis will not work on zoneddatetime
                            newdate.setSeconds(0);
                            tenMinAvgDoc = new Document("date", newdate)
                              .append("device", device)
                              .append("poa", w21w22avg)
                              .append("modtemp", w10w14avg); 
                            
                            wmsDoc10MinL.add(tenMinAvgDoc);
                        }

                        w21=0.0f;
                        w22=0.0f;
                        w10=0.0f;
                        w14=0.0f;
                        w21c=0;
                        w22c=0;
                        w10c=0;
                        w14c=0;
                        sampleC=0;
                    }
                }
            
                try{
                    if(keyset.contains("w21")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w21").toString());
                        if(!Float.isNaN(valueHolderF)){
                            if(valueHolderF>0){
                                w21=w21+valueHolderF;
                                w21c++;
                            }
                            else{
                                continue;
                            }
                        }
                        else{
                            continue;
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch w21");
                }

                try{
                    if(keyset.contains("w22")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w22").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w22=w22+valueHolderF;
                            w22c++;
                        }
                    } 
                }
                catch (Exception ex) {
                    System.out.println("catch w22");
                }
                
                try{
                    if(keyset.contains("w10")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w10").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w10=w10+valueHolderF;
                            w10c++;
                        }
                    } 
                }
                catch (Exception ex) {
                    System.out.println("catch w10");
                }
                
                try{
                    if(keyset.contains("w14")){
                        valueHolderF=Float.parseFloat(wmsDoc.get("w14").toString());
                        if(!Float.isNaN(valueHolderF)){
                            w14=w14+valueHolderF;
                            w14c++;
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch w14");
                }
                sampleC++;
            }
            wmsDoc10MinLL.add(wmsDoc10MinL);
        }

        List<Date> wmsStartDateL=new ArrayList<>();
        Date wmsStartDate=new Date();
        for(List<Document> listd:wmsDoc10MinLL){
            for(Document d:listd){
                wmsStartDateL.add(d.getDate("date"));
                break;
            }
        }

        for(Date d:wmsStartDateL){
            if(d.compareTo(wmsStartDate)<0){
                wmsStartDate=d;
            }
        }
        wmsStartDate.setSeconds(0);
        
        //for MFM
        float m32=0.0f;
        int m32c=0;
        List<Document> mfmDoc10MinL=new ArrayList<>();
        for(List<Document> mfmDocMongoL:mfmDocMongoLL){
            mfmDoc10MinL=new ArrayList<>();
            for(Document mfmDoc:mfmDocMongoL){
                String device=mfmDoc.getString("device");
                keyset=mfmDoc.keySet();
                newdate=mfmDoc.getDate("ts");
                ZonedDateTime date=newdate.toInstant().atZone(ZoneId.of(siteZone));
                if(!(date.getHour()==0 && date.getMinute()==0)){
                    if((date.getMinute()%60)==0){
//                        if(m32>0){
//                            m32=m32/m32c;
//                        }
                        //m32c is for only understanding th ecount if energy lower
                        if((mfmDoc.getDate("ts").compareTo(wmsStartDate)>=0) && sampleC==60){
                            //compaerision will be on date basis will not work on zoneddatetime
                            newdate.setSeconds(0);
                            tenMinAvgDoc = new Document("date", newdate)
                              .append("device", device)
                              .append("count", sampleC)
                              .append("m32c", m32c)
                              .append("m32", m32);
                            
                            mfmDoc10MinL.add(tenMinAvgDoc);
                        }

                        m32=0.0f;
                        m32c=0;
                        sampleC=0;
                    }
                }
            
                try{
                    if(keyset.contains("m32")){
                        valueHolderF=Float.parseFloat(mfmDoc.get("m32").toString());
                        if(!Float.isNaN(valueHolderF)){
                            m32=m32+valueHolderF;
                            m32c++;
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch m32");
                }
                sampleC++;
            }
            mfmDoc10MinLL.add(mfmDoc10MinL);
        }
        
        List<Float> CPR=new ArrayList<>();
        for(Document d:wmsDoc10MinLL.get(0)){
            poa=0.0f;
            modtemp=0.0f;
            poaC=0;
            modtempC=0;
            activePow=0.0f;
            cpr=0.0f;
            cprC=0;
            newdate=d.getDate("date");
            try{
                poa=Float.parseFloat(d.get("poa").toString());
                poaC++;
            }
            catch (Exception ex) {
                System.out.println("catch actualestimation 10minpr poa");
            }
            try{
                modtemp=Float.parseFloat(d.get("modtemp").toString());
                modtempC++;
            }
            catch (Exception ex) {
                System.out.println("catch actualestimation 10minpr modtemp");
            }
            
            for(int i=1;i<wmsDoc10MinLL.size();i++){
                for(Document d2:wmsDoc10MinLL.get(i)){
                    if(d2.getDate("date").getHours()==newdate.getHours() && d2.getDate("date").getMinutes()==newdate.getMinutes()){
                        try{
                            poa=poa+Float.parseFloat(d2.get("poa").toString());
                            poaC++;
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 10minpr w21w22avg");
                        }
                        try{
                            modtemp=modtemp+Float.parseFloat(d2.get("modtemp").toString());
                            modtempC++;
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 10minpr w10w14avg");
                        }
                        break;
                    }
                }
            }
            
            for(int i=0;i<mfmDoc10MinLL.size();i++){
                for(Document d3:mfmDoc10MinLL.get(i)){
                    if(d3.getDate("date").getHours()==newdate.getHours() && d3.getDate("date").getMinutes()==newdate.getMinutes()){
                        try{
                            valueHolderF=Float.parseFloat(d3.get("m32").toString());
                            if(!Float.isNaN(valueHolderF)){
                                activePow=activePow+valueHolderF;
                            }
                        }
                        catch (Exception ex) {
                            System.out.println("catch actualestimation 10minpr m32");
                        }
                        break;
                    }
                }
            }
            estimModTempHourly=estimModTempHourlyList.get(newdate.getHours());
            if(dcCapacity>0 && poa>0){
                cpr=cpr+((((activePow/60)/dcCapacity)*(1/(1-(modTempCoeff*(estimModTempHourly-(modtemp/modtempC))))))/(poa/(poaC*1000)))*100;
                cprC++;
            }
        }
        
        if(cprCondition && cprC>1){
            cpr=cpr/cprC;
        }
        else{
            cpr=-1;
        }
        
        try{
            stmt1.close();
            stmt2.close();
            stmt3.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }
        return cpr;
    }
    
    
    public float cPA1(String siteID, String siteZone, Connection conn, float plantWeight, float invWeight, float smbWeight, float stringWeight) throws SQLException, ParseException {
        //Plant, inverter, string level breakdown
        String todayD=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currDT=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        String wmsDailyQ=String.format("SELECT wdaily.wms_id,wdaily.poa_irradiance,wdaily.today_date,wdaily.plant_sunrise_time,wdaily.plant_sunset_time,wdaily.sunrise_Time_updated,wdaily.sunset_Time_updated FROM `Flexi_solar_wms_list` AS wlist JOIN `Flexi_solar_wms_daily` AS wdaily ON wdaily.plant_id = wlist.plant_id AND wdaily.wms_id=wlist.wms_id WHERE wlist.plant_id=%s AND wlist.WMS_Cumulative=1 AND wdaily.Today=%s;", siteID, todayD);  
        String eventDetQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_events where plant_id=%s and device_type in ('Plant', 'Inverter', 'SMB', 'String') and category not in (1,3,5) and error_code in (1000,2001,3000,7000) and (start_today=%s or end_today=%s or end_today is null);", siteID, todayD, todayD);
        ResultSet wmsDailyRs=stmt1.executeQuery(wmsDailyQ);
        ResultSet rsEvent=stmt2.executeQuery(eventDetQ);
        
        float cpa=100f;
        Date sunriseTime=null;
        Date sunsetTime=null;
        long totMin=0;
        long plantBDMin=0;
        long invBDMin=0;
        long smbBDMin=0;
        long stringBDMin=0;
        float valueHolderF=0.0f;
        
        while(wmsDailyRs.next()){
            try{
                if(wmsDailyRs.getString("wms_id").contentEquals("1") && wmsDailyRs.getString("sunrise_Time_updated").contentEquals("1")){
                    try {
                        sunriseTime=dateFormat.parse(wmsDailyRs.getString("plant_sunrise_time"));
                    } catch (Exception ex) {
                        System.out.println("Catch inverter daily 1");
                    }
                }
            }
            catch (Exception ex) {    
            }
            
            try{
                if(wmsDailyRs.getString("wms_id").contentEquals("1") && wmsDailyRs.getString("sunset_Time_updated").contentEquals("0")){
                    try {
                        sunsetTime=dateFormat.parse(wmsDailyRs.getString("plant_sunset_time"));
                    } catch (Exception ex) {
                        System.out.println("Catch inverter daily 2");
                    }
                }
            }
            catch (Exception ex) {
            }
            
        }
        
        try {
            if(sunriseTime==null){
                cpa=0.0f;
            }
            else if(sunriseTime!=null && sunsetTime==null){
                totMin=((dateFormat.parse(currDT)).getTime()-sunriseTime.getTime())/(60*1000);
            }
            else if(sunriseTime!=null && sunsetTime!=null){
                totMin=((sunsetTime).getTime()-sunriseTime.getTime())/(60*1000);
            }
        }
        catch (Exception ex) {
            System.out.println("catch inverteravaratio");
        }
        
        rsEvent.beforeFirst();
        while(rsEvent.next()){
            if(sunriseTime!=null){
                //add in breakdown time
                try{
                    if(sunsetTime==null && rsEvent.getString("end_date")==null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+((dateFormat.parse(currDT).getTime()-(dateFormat.parse(rsEvent.getString("start_date")).getTime()))/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+((dateFormat.parse(currDT).getTime()-(dateFormat.parse(rsEvent.getString("start_date")).getTime()))/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+((dateFormat.parse(currDT).getTime()-(dateFormat.parse(rsEvent.getString("start_date")).getTime()))/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+((dateFormat.parse(currDT).getTime()-(dateFormat.parse(rsEvent.getString("start_date")).getTime()))/(60*1000));
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+((dateFormat.parse(currDT).getTime()-(sunriseTime).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+((dateFormat.parse(currDT).getTime()-(sunriseTime).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+((dateFormat.parse(currDT).getTime()-(sunriseTime).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+((dateFormat.parse(currDT).getTime()-(sunriseTime).getTime())/(60*1000));
                            }
                        }
                    }
                    else if(sunsetTime==null && rsEvent.getString("end_date")!=null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunriseTime)>=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(sunriseTime).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(sunriseTime).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(sunriseTime).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(sunriseTime).getTime())/(60*1000));
                            }
                        }
                    }
                    else if(sunsetTime!=null && rsEvent.getString("end_date")==null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunsetTime)<=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            }
                        }
                    }
                    else if(sunsetTime!=null && rsEvent.getString("end_date")!=null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)<=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunsetTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)>=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunriseTime)>=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-sunriseTime.getTime())/(60*1000));
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)>=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                plantBDMin=plantBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("Inverter")){
                                invBDMin=invBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("SMB")){
                                smbBDMin=smbBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            }
                            if(rsEvent.getString("device_type").contentEquals("String")){
                                stringBDMin=stringBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch bdmino");
                }
            }
        }

        if(totMin>0.0){
            cpa=((totMin-(((plantBDMin*100)+(invBDMin*invWeight)+(smbBDMin*smbWeight)+(stringBDMin*stringWeight))/100))/totMin)*100;
        }
        try{
            stmt1.close();
            stmt2.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }
        return cpa;
    }
    
    
    public float cPA2(float estimatedPR, float kwhTodayExport, String siteID, String siteZone, MongoCollection mongocollection, Connection conn) throws SQLException, ParseException {
        //Plant, inverter, string level breakdown
        String todayD=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone(siteZone));
        String currDT=ZonedDateTime.now(ZoneId.of(siteZone)).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        
        Statement stmt1= conn.createStatement();
        Statement stmt2= conn.createStatement();
        String wmsDailyQ=String.format("SELECT wdaily.wms_id,wdaily.poa_irradiance,wdaily.today_date,wdaily.plant_sunrise_time,wdaily.plant_sunset_time,wdaily.sunrise_Time_updated,wdaily.sunset_Time_updated FROM `Flexi_solar_wms_list` AS wlist JOIN `Flexi_solar_wms_daily` AS wdaily ON wdaily.plant_id = wlist.plant_id AND wdaily.wms_id=wlist.wms_id WHERE wlist.plant_id=%s AND wlist.WMS_Cumulative=1 AND wdaily.Today=%s;", siteID, todayD);  
        String eventDetQ=String.format("SELECT * FROM swlflexi_CMSswlawsReMACS.Flexi_solar_events where plant_id=%s and device_type in ('Plant', 'Inverter', 'SMB', 'String') and category not in (1,3,5) and error_code in (1000,2001,3000,7000) and (start_today=%s or end_today=%s or end_today is null);", siteID, todayD, todayD);
        ResultSet wmsDailyRs=stmt1.executeQuery(wmsDailyQ);
        ResultSet rsEvent=stmt2.executeQuery(eventDetQ);
        
        float cpa=100f;
        Date sunriseTime=null;
        Date sunsetTime=null;
        float lossEnergy=0.0f;
        String valueHolderS;
        float valueHolderF=0.0f;
        
        while(wmsDailyRs.next()){
            try{
                if(wmsDailyRs.getString("wms_id").contentEquals("1") && wmsDailyRs.getString("sunrise_Time_updated").contentEquals("1")){
                    try {
                        sunriseTime=dateFormat.parse(wmsDailyRs.getString("plant_sunrise_time"));
                    } catch (Exception ex) {
                        System.out.println("Catch inverter daily 1");
                    }
                }
            }
            catch (Exception ex) {    
            }
            
            try{
                if(wmsDailyRs.getString("wms_id").contentEquals("1") && wmsDailyRs.getString("sunset_Time_updated").contentEquals("0")){
                    try {
                        sunsetTime=dateFormat.parse(wmsDailyRs.getString("plant_sunset_time"));
                    } catch (Exception ex) {
                        System.out.println("Catch inverter daily 2");
                    }
                }
            }
            catch (Exception ex) {
            }
            
        }

        rsEvent.beforeFirst();
        while(rsEvent.next()){
            if(sunriseTime!=null){
                //add in breakdown time
                try{
                    if(sunsetTime==null && rsEvent.getString("end_date")==null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0){
                            //plantBDMin=plantBDMin+((dateFormat.parse(currDT).getTime()-(dateFormat.parse(rsEvent.getString("start_date")).getTime()))/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("start_date")).toInstant(), ZoneOffset.UTC).toInstant()), (ZonedDateTime.ofInstant(dateFormat.parse(currDT).toInstant(), ZoneOffset.UTC).toInstant()), siteID, mongocollection, conn)*(Float.parseFloat(rsEvent.getString("dc_capacity")))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<0){
                            //plantBDMin=plantBDMin+((dateFormat.parse(currDT).getTime()-(sunriseTime).getTime())/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(sunriseTime.toInstant(), ZoneOffset.UTC).toInstant()), (ZonedDateTime.ofInstant(dateFormat.parse(currDT).toInstant(), ZoneOffset.UTC).toInstant()), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                    }
                    else if(sunsetTime==null && rsEvent.getString("end_date")!=null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0){
                            //plantBDMin=plantBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("start_date")).toInstant(), ZoneOffset.UTC).toInstant()), (ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("end_date")).toInstant(), ZoneOffset.UTC).toInstant()), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunriseTime)>=0){
                            //plantBDMin=plantBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(sunriseTime).getTime())/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(sunriseTime.toInstant(), ZoneOffset.UTC).toInstant()), (ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("end_date")).toInstant(), ZoneOffset.UTC).toInstant()), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                    }
                    else if(sunsetTime!=null && rsEvent.getString("end_date")==null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunsetTime)<=0){
                            //plantBDMin=plantBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("start_date")).toInstant(), ZoneOffset.UTC).toInstant()), (ZonedDateTime.ofInstant(sunsetTime.toInstant(), ZoneOffset.UTC).toInstant()), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0){
                            //plantBDMin=plantBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(sunriseTime.toInstant(), ZoneOffset.UTC).toInstant()), (ZonedDateTime.ofInstant(sunsetTime.toInstant(), ZoneOffset.UTC).toInstant()), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                    }
                    else if(sunsetTime!=null && rsEvent.getString("end_date")!=null){
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)<=0){
                            //plantBDMin=plantBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("start_date")).toInstant(), ZoneOffset.UTC).toInstant()), (ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("end_date")).toInstant(), ZoneOffset.UTC).toInstant()), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)>=0 && dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunsetTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)>=0){
                            //plantBDMin=plantBDMin+((sunsetTime.getTime()-(dateFormat.parse(rsEvent.getString("start_date"))).getTime())/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("start_date")).toInstant(), ZoneOffset.UTC).toInstant()), ZonedDateTime.ofInstant(sunsetTime.toInstant(), ZoneOffset.UTC).toInstant(), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunriseTime)>=0){
                            if(rsEvent.getString("device_type").contentEquals("Plant")){
                                //plantBDMin=plantBDMin+(((dateFormat.parse(rsEvent.getString("end_date"))).getTime()-sunriseTime.getTime())/(60*1000));
                                try{
                                    valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(sunriseTime.toInstant(), ZoneOffset.UTC).toInstant()), ZonedDateTime.ofInstant(dateFormat.parse(rsEvent.getString("end_date")).toInstant(), ZoneOffset.UTC).toInstant(), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                                }
                                catch(Exception ex){
                                    System.out.println("Catch in cpa2 "+siteID);
                                }
                                if(!Float.isNaN(valueHolderF)){
                                    lossEnergy=lossEnergy+valueHolderF;
                                }
                            }
                        }
                        if(dateFormat.parse(rsEvent.getString("start_date")).compareTo(sunriseTime)<=0 && dateFormat.parse(rsEvent.getString("end_date")).compareTo(sunsetTime)>=0){
                            //plantBDMin=plantBDMin+((sunsetTime.getTime()-sunriseTime.getTime())/(60*1000));
                            try{
                                valueHolderF= getdata.irradianceBtw((ZonedDateTime.ofInstant(sunriseTime.toInstant(), ZoneOffset.UTC).toInstant()), ZonedDateTime.ofInstant(sunsetTime.toInstant(), ZoneOffset.UTC).toInstant(), siteID, mongocollection, conn)*Float.parseFloat(rsEvent.getString("dc_capacity"))*estimatedPR;
                            }
                            catch(Exception ex){
                                System.out.println("Catch in cpa2 "+siteID);
                            }
                            if(!Float.isNaN(valueHolderF)){
                                lossEnergy=lossEnergy+valueHolderF;
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    System.out.println("catch lossenergy");
                }
            }
        }
        if(lossEnergy>0.0){
            cpa=(kwhTodayExport/(kwhTodayExport+lossEnergy))*100;
        }
        
        try{
            stmt1.close();
            stmt2.close();
        }
        catch(Exception ex){
            System.out.println("Catch stmt1close");
        }

        return cpa;
    }
        
}

