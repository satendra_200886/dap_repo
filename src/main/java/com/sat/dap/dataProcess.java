package com.sat.dap;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.regex;
import static com.mongodb.client.model.Indexes.ascending;
import static com.mongodb.client.model.Indexes.descending;
import static com.mongodb.client.model.Sorts.orderBy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.Document;


public class dataProcess {

    public void deviceProcess(String sitesID, MongoClient mongoclient, Connection conn) {
        getData getdata=new getData();
        dailyParamProcess dailyparamprocess=new dailyParamProcess();
        dailyActual dailyactual= new dailyActual();
        monthlyActual monthlyactual=new monthlyActual();
        yearlyActual yearlyactual=new yearlyActual();

        String[] records=sitesID.split(";");
        String siteName=records[0];
        String siteID=records[1];
        String siteDB=records[2];
        String siteZone=records[3];
        String regexS="";
        
        MongoDatabase mongodatabse=null;
        MongoCollection<Document> mongocollection=null;
        try{
            mongodatabse = mongoclient.getDatabase(siteDB);
            mongocollection= mongodatabse.getCollection("payloads");
        }
        catch (Exception ex) {
            System.out.println("Catch Dataprocess mongoconnection "+siteName);
        }
        
        String datetime=ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"));
        
        DateTimeFormatter mongoDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Instant timeInterval=ZonedDateTime.now(ZoneOffset.UTC).minusMinutes(4).withSecond(0).toInstant();
        Instant todayStartDT = LocalDate.parse(datetime, mongoDateFormatter)
            .atStartOfDay(ZoneId.of(siteZone))
            .toInstant();

        List<Document> wmsdocListLatestMongo=new ArrayList<>();
        List<Document> mfmdocListLatestMongo=new ArrayList<>();
        List<Document> docListLatestMongo=new ArrayList<>();

        //for wms
        try{
            regexS=String.format("%s_WMS",siteID);
            wmsdocListLatestMongo=getdata.mongoDevGteData(regexS, timeInterval, mongocollection, conn, siteID);
            if(wmsdocListLatestMongo.size()>0){
                dailyparamprocess.wmsDaily(wmsdocListLatestMongo, siteID, siteZone, conn, mongocollection, todayStartDT);
            }
        }
        catch (Exception ex) {
            System.out.println("catch wms dataprocess "+siteID);
        }    

        //for MFM
        try{
            regexS=String.format("%s_MFM",siteID);
            mfmdocListLatestMongo=getdata.mongoDevGteData(regexS, timeInterval, mongocollection, conn, siteID);
            if(mfmdocListLatestMongo.size()>0){
                dailyparamprocess.mfmDaily(mfmdocListLatestMongo, siteID, siteZone, conn, mongocollection, todayStartDT);
            }
        }
        catch (Exception ex) {
            System.out.println("catch mfm dataprocess "+siteID);
        }
        
        //for inverter
        try{
            regexS=String.format("%s_Inverter",siteID);
            docListLatestMongo=getdata.mongoDevGteData(regexS, timeInterval, mongocollection, conn, siteID);
            if(docListLatestMongo.size()>0){
                dailyparamprocess.inverterDaily(docListLatestMongo, siteID, siteZone, conn, mongocollection, todayStartDT);
            }
        }
        catch (Exception ex) {
            System.out.println("catch inverter dataprocess "+siteID);
        }
        
        //for SMB
        try{
            regexS=String.format("%s_SMB",siteID);
            docListLatestMongo=getdata.mongoDevGteData(regexS, timeInterval, mongocollection, conn, siteID);
            if(docListLatestMongo.size()>0){
                dailyparamprocess.smbDaily(docListLatestMongo, siteID, siteZone, conn, mongocollection, todayStartDT);
            }
        }
        catch (Exception ex) {
            System.out.println("catch smb dataprocess "+siteID);
        }

        //System.out.println("size "+mfmdocListLatestMongo.size()+" "+wmsdocListLatestMongo.size()+" "+docListLatestMongo.size());
        try{
            if(wmsdocListLatestMongo.size()>0 && mfmdocListLatestMongo.size()>0){
                dailyactual.dailyParam(docListLatestMongo, siteID, siteZone, conn, mongocollection, todayStartDT);
            }
        }
        catch (Exception ex) {
            System.out.println("catch actualdaily dataprocess "+siteID);
        }
        
        if(siteID.contentEquals("19") || siteID.contentEquals("20") || siteID.contentEquals("21") || siteID.contentEquals("39") || siteID.contentEquals("141") || siteID.contentEquals("142") || siteID.contentEquals("41") || siteID.contentEquals("42") || siteID.contentEquals("137") || siteID.contentEquals("11")){
            try{
                monthlyactual.monthlyPfactor(siteID, siteZone, conn);
            }
            catch (Exception ex) {
                System.out.println("catch mailymonthly dataprocess "+siteID);
            }

            try{
                yearlyactual.yearlyPfactor(siteID, siteZone, conn);
            }
            catch (Exception ex) {
                System.out.println("catch yearly dataprocess "+siteID);
            }
        }
        
    }
    
    
}