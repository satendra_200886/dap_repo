package com.sat.dap;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.regex;
import static com.mongodb.client.model.Indexes.descending;
import static com.mongodb.client.model.Sorts.orderBy;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.Document;

public class connection {
    
    public List<String> siteInfo(String currDir) throws IOException{
        String mapFile=currDir+"/SiteInfo.csv";
        BufferedReader bR=null;
        List<String> mapLines = new ArrayList<>();
        try {
            bR = new BufferedReader(new FileReader(new File(mapFile)));
            String line;
            while ((line = bR.readLine()) != null) {
                if (!(line.trim().equals(""))) {
                    mapLines.add(line);
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            bR.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return(mapLines);
    }
    
    
    public MongoClient mongoConnection(){
        final String mongouri= "mongodb+srv://lambda-read:wUr7T4b93HnaXKe8@cms-production-eopob.mongodb.net/test?retryWrites=true";
        MongoClient mongoclient= MongoClients.create(mongouri);
        return mongoclient;
    }
    
    public Connection sqlConnection(){
        final String sqluri= "jdbc:mysql://15.206.60.12:3306/swlflexi_CMSswlawsReMACS";
        final String sqlid= "devuser";
        final String sqlpass= "dev@sterlingandwilson";
        Connection conn=null;
        try {
            conn = DriverManager.getConnection(sqluri,sqlid,sqlpass);
        } catch (SQLException ex) {
            Logger.getLogger(SiteExec.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }
    
    
}
