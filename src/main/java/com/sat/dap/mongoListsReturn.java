
package com.sat.dap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.bson.Document;

public class mongoListsReturn {
    private final List<Document> docList;
    private final List<Document> docListLatest;

    public mongoListsReturn(List<Document> docList, List<Document> docListLatest) {
        this.docList= new ArrayList<>(docList);
        this.docListLatest= new ArrayList<>(docListLatest);
    }
    public List<Document> getdocList() {
        return docList;
    }
    public List<Document> getdocListLatest() {
        return docListLatest;
    }
    
}
